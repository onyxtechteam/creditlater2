

            <!-- start content -->
            <div class="content-page">
                <div class="content">
                    
                    <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row page-title">
                            <div class="col-md-12">
                                <h4 class="mb-1 mt-0">Customer</h4>
                            </div>
                        </div>
                      
                        <div class="row">
                            <div class="col-12">
                                <div class="add_user_alert alert-success p-3 mb-2 text-center" style="display: none;"></div>
                                <div class="card get_customer_table_ajax">
                                    <div class="table-responsive table-striped p-3">
                                        <h4 class="header-title mt-0 mb-1">Customer Table <a href="#" data-toggle="modal" data-target="#add_customer_modal" class="float-right btn btn-primary">Add customer</a></h4>
                                        <!-- <p class="sub-header"></p> -->
                                        <table id="basic-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Last login</th>
                                                    <th>Joined</th>
                                                    <th>Deactivate</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($customers) != '')
                                            @foreach($customers as $customer)
                                               <tr>
                                                    <td>
                                                       @if($customer->user_image)
                                                       <img src="{{ asset($customer->user_image) }}" alt="{{ $customer->first_name }}" class="admin_image {{ $customer->active ? 'activate' : 'deactivate' }}">
                                                       @endif
                                                    </td>
                                                   <td>{{ $customer->first_name }}</td>
                                                   <td>{{ $customer->email }}</td>
                                                   <td>{{ date('d M Y', strtotime($customer->last_login)) }}</td>
                                                   <td>{{ date('d M Y', strtotime($customer->date_joined)) }}</td>
                                                   <td>
                                                        <div class="custom-control custom-checkbox mb-2">
                                                            <input type="checkbox" data-url="{{ url('/admin/deactivate-customer') }}" class="custom-control-input customer_deactivate_input" id="deactivate_{{ $customer->id }}" {{ $customer->is_deactivate ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="deactivate_{{ $customer->id }}"></label>
                                                        </div> 
                                                   </td>
                                                   <td>
                                                       <a href="{{ url('/admin/get-edit-customer-detail') }}" id="{{ $customer->id }}" data-image="{{ asset($customer->user_image) }}" class="icon edit_customer_modal_btn" data-toggle="modal" data-target="#edit_customer_modal"><i class="fa fa-pencil"></i></a>
                                                       <span style="padding: 0 10px;"></span>
                                                       <a href="#" id="{{ $customer->id }}" data-toggle="modal" data-target="#delete_customer_modal" class="icon delete_customer_modal"><i class="fa fa-trash"></i></a>
                                                   </td>
                                               </tr>
                                               @endforeach
                                               @endif
                                            </tbody>
                                        </table>
                                    </div> <!-- end card body-->
                                    @if(count($customers) == '')
                                    <div class="p-3 text-center">There are no Customer Available</div>
                                    @endif
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->
                        <div class="pagination"> {{ $customers->links("pagination::bootstrap-4") }}</div>
                    </div> <!-- container-fluid -->

                </div> 
                <!-- content -->



<!-- ADD CUSTOMER MODAL -->
<div id="add_customer_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Add Customer</h5>
                <button type="button" class="close" id="add_user_modal_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" class="confirm_add_customer_form">
               <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_1 text-danger"></div>
                            <label for="label">First name:</label>
                            <input type="text" id="customer_firstName_input" class="form-control" value="" placeholder="First name">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_2 text-danger"></div>
                            <label for="label">Last name:</label>
                            <input type="text" id="customer_lastName_input" class="form-control" value="" placeholder="Last name">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_3 text-danger"></div>
                            <label for="label">Email:</label>
                            <input type="email" id="customer_email_input" class="form-control" value="" placeholder="Email">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_4 text-danger"></div>
                            <label for="label">Password:</label>
                            <input type="text" id="customer_password_input" class="form-control" value="" placeholder="Password">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_5 text-danger"></div>
                            <label for="label">Confirm password:</label>
                            <input type="text" id="customer_confirm_password_input" class="form-control" value="" placeholder="Confirm password">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                <a href="{{ url('/admin/add-customer') }}" id="add_customer_btn" class="btn btn-primary">Add customer</a>
                        </div> 
                    </div>
            </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ADD CUSTOMER MODAL END -->






<!-- EDIT CUSTOMER MODAL -->
<div id="edit_customer_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Edit Customer</h5>
                <button type="button" class="close" id="edit_user_modal_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- preloader -->
            <div class="content_preloader">
                <img src="{{ asset('admins/images/ajax-loader.gif') }}" alt="">
            </div>
            <!-- end preloader -->

            <form action="" method="post" class="confirm_add_customer_form">
               <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="customer_image text-center">
                                <div class="inner_image_container">
                                    <a href="{{ url('/admin/delete-customer-image-ajax') }}" id="" class="delete_customer_image">
                                        <i class="fa fa-trash" title="Delete image"></i>
                                    </a>
                                    <img src="{{ asset('admins/images/users/demo.png') }}" alt="" class="admin_image_modal">
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_1 text-danger"></div>
                            <label for="label">First name:</label>
                            <input type="text" id="edit_customer_firstName_input" class="form-control" value="" placeholder="First name">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_2 text-danger"></div>
                            <label for="label">Last name:</label>
                            <input type="text" id="edit_customer_lastName_input" class="form-control" value="" placeholder="Last name">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_3 text-danger"></div>
                            <label for="label">Email:</label>
                            <input type="email" id="edit_customer_email_input" class="form-control" value="" placeholder="Email">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_4 text-danger"></div>
                            <label for="label">Password:</label>
                            <input type="text" id="edit_customer_password_input" class="form-control" value="" placeholder="Password">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_5 text-danger"></div>
                            <label for="label">Confirm password:</label>
                            <input type="text" id="edit_customer_confirm_password_input" class="form-control" value="" placeholder="Confirm password">
                        </div> 
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="modal-body">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                <a href="{{ url('/admin/edit-customer') }}" id="" class="btn btn-primary edit_customer_btn">Edit customer</a>
                        </div> 
                    </div>
            </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- EDIT CUSTOMER MODAL END -->




<!-- Customer  Delete confirmation -->
<div class="modal fade" id="delete_customer_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title text-center text-danger" id="">Do you wish to delete this Customer?</p>
        <button type="button" class="close delete_customer_modal_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_brand_form">
        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a  href="{{ url('/admin/delete-customer-ajax') }}"  class="btn btn-primary confirm_customer_delete_submit" id="">Delete</a>
      </div>
      </form>
    </div>
  </div>
</div>






<!-- get all customers ajax url-->
<a href="{{ url('/admin/get-all-customers-ajax') }}" class="get_all_customers_ajax_url" style="display: none;"></a>



<script>
$(document).ready(function(){
    
// ADD CUSTOMER
$('#add_customer_btn').click(function(e){
    e.preventDefault();
    var url = $(this).attr('href');

    csrf_token() // gets page unique token

    add_customer(url);
});



function add_customer(url){
    var first_name = $("#customer_firstName_input").val();
    var last_name = $("#customer_lastName_input").val();
    var email = $("#customer_email_input").val();
    var password = $("#customer_password_input").val();
    var confirm_password = $("#customer_confirm_password_input").val();

    var data = new FormData();
    reset_alert() //reset alert

    data.append('first_name', first_name);
    data.append('last_name', last_name);
    data.append('email', email);
    data.append('password', password);
    data.append('confirm_password', confirm_password);

    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
           if(response.error)
           {
               asign_alert(response.error)
           }else if(response.data){
               get_all_customers(); //get all customers
               $("#add_user_modal_close").click();
               $('.add_user_alert').html('The customer '+first_name+' '+last_name+' has been added successfully!');
               $('.add_user_alert').show();
               remove_success_alert();
           }
        }
    });
}



// CSRF PAGE TOKEN
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}


function remove_success_alert(){
    setTimeout(function(){
       $(".add_user_alert").hide();
    }, 4000);
    reset_alert();
    reset_add_customer_input_fields()
}

function reset_alert(){
    $(".alert_input_1").html('');
    $(".alert_input_2").html('');
    $(".alert_input_3").html('');
    $(".alert_input_4").html('');
    $(".alert_input_5").html('');
}

function asign_alert(error){
    $(".alert_input_1").html(error.first_name);
    $(".alert_input_2").html(error.last_name);
    $(".alert_input_3").html(error.email);
    $(".alert_input_4").html(error.password);
    $(".alert_input_5").html(error.confirm_password);
}



function get_all_customers()
{
    var url = $(".get_all_customers_ajax_url").attr('href');

    csrf_token() // gets page unique token

     $.ajax({
        url: url,
        method: "post",
        data: {
            get_customers: 'get_customers'
        },
        success: function (response){
            $('.get_customer_table_ajax').html(response);
        }
    });
}



// ======================================================
// EDIT CUSTOMER MODAL  BUTTON
// ======================================================
$(".edit_customer_modal_btn").click(function(e){
    var url = $(this).attr('href');
    var id = $(this).attr('id');
    var img = $(this).attr('data-image');
    var demo_image = 'admins/images/users/demo.png';
    $('.delete_customer_image').show();
         

    $(".content_preloader").show();
    $('.edit_customer_btn').attr('id', id);

    csrf_token() // gets page unique token

    $.ajax({
        url: url,
        method: "post",
        data: {
        id: id
        },
        success: function (response){
            if(response.data){
                $("#edit_customer_firstName_input").val(response.data.first_name);
                $("#edit_customer_lastName_input").val(response.data.last_name);
                $("#edit_customer_email_input").val(response.data.email);
                $('.delete_customer_image').attr('id', id);
                $(".admin_image_modal").attr('src', img);

                if(response.data.user_image == demo_image){
                    $('.delete_customer_image').hide();
                }
                remove_preloader(); //remove preloader
            }
        }
    });
    
});





function reset_add_customer_input_fields(){
    $("#customer_firstName_input").val('');
    $("#customer_lastName_input").val('');
    $("#customer_email_input").val('');
    $("#customer_password_input").val('');
    $("#customer_confirm_password_input").val('');
}






// REMOVE PRELOADER 
function remove_preloader()
{
    setTimeout(function(){
        $(".content_preloader").hide();
    }, 1500);
}




// EDIT CUSTOMERS AUTH DETAILS
$('.edit_customer_btn').click(function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');

    edit_customer(url, id);
});



function edit_customer(url, id){
    var first_name = $("#edit_customer_firstName_input").val();
    var last_name = $("#edit_customer_lastName_input").val();
    var email = $("#edit_customer_email_input").val();
    var password = $("#edit_customer_password_input").val();
    var confirm_password = $("#edit_customer_confirm_password_input").val();

    var data = new FormData();
    reset_alert() //reset alert

    data.append('id', id);
    data.append('first_name', first_name);
    data.append('last_name', last_name);
    data.append('email', email);
    data.append('password', password);
    data.append('confirm_password', confirm_password);

    csrf_token() // gets page unique token

    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
            if(response.error)
            {
                asign_alert(response.error);
            }else if(response.data){
                get_all_customers(); //get all customers
                $("#edit_user_modal_close").click();
                $('.add_user_alert').html('Customer has been updated successfully!');
                $('.add_user_alert').show();
                remove_success_alert();
            }
        }
    });
}




// REMOVE CUSTOMER IMAGE
$('.customer_image').on('click', '.delete_customer_image', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');
    
    csrf_token() // gets page unique token

    $.ajax({
        url: url,
        method: "post",
        data: {
        id: id
        },
        success: function (response){
           $('.customer_image').html(response);
           $('.delete_customer_image').hide();
        }
    });
});






// GET CUSTOMER DELETE MODAL
$('.get_customer_table_ajax').on('click', '.delete_customer_modal', function(e){
    var id = $(this).attr('id');
    $(".confirm_customer_delete_submit").attr('id', id);
});

// ========================================
// DELETE CUSTOMER
// ========================================
$(".confirm_customer_delete_submit").click(function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');

     csrf_token() // gets page unique token

    $.ajax({
        url: url,
        method: "post",
        data: {
        id: id
        },
        success: function (response){
           if(response.data){
               get_all_customers(); //get all customers
               $(".delete_customer_modal_close").click();
               $('.add_user_alert').html('Customer has been deleted successfully!');
               $('.add_user_alert').show();
               remove_success_alert();
           }
        }
    });

});




// =================================================
// DEACTIVATE CUSTOMER
// =================================================
$(".get_customer_table_ajax").on('click', '.customer_deactivate_input', function(e){
    var id = $(this).attr('id');
    var url = $(this).attr('data-url');

    csrf_token() // gets page unique token

    $.ajax({
        url: url,
        method: "post",
        data: {
        id: id
        },
        success: function (response){
           if(response.data){
               get_all_customers(); //get all customers
           }
        }
    });
});













































// end
});
</script>