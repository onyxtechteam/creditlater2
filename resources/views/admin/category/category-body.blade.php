

            <!-- start content -->
            <div class="content-page">
                <div class="content">
                    
                    <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row page-title">
                            <div class="col-md-12">
                                <h4 class="mb-1 mt-0">Category</h4>
                            </div>
                        </div>
                        @if($categories)
                        <div class="row">
                            <div class="col-12">
                                <div class="add_alert_category bg-success p-3 mb-2 text-center" style="color: #fff; display: none;"></div>
                                <div class="card get_category_ajax">
                                    <div class="table-responsive table-striped p-3">
                                        <h4 class="header-title mt-0 mb-1">Categories Table <a href="#" data-toggle="modal" data-target="#exampleModal_add_category" class="float-right btn btn-primary">Add Category</a></h4>
                                        <!-- <p class="sub-header"></p> -->
                                            <table id="basic-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Image</th>
                                                    <th>Featured</th>
                                                    <th>Date Added</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                        
                                            <tbody>
                                                @foreach($categories as $category)
                                                <tr>
                                                    <td><a href="#" style="color: #717171">{{ $category->category_name }}</a></td>
                                                    <td><a href="#"><img src="{{ asset($category->category_image) }}" alt="image"></a></td>
                                                    <td><a href="{{ url('/admin/category-feature') }}" id="{{ $category->category_id }}" class="category_feature_ajax"><i class="fa fa-{{ $category->is_feature ? 'check text-success' : 'times text-danger' }}"></i></a></td>
                                                    <td>{{ date('d M Y', strtotime($category->date_added)) }}</td>
                                                    <td><a href="{{ url('/admin/edit-category-ajax') }}" style="color: #717171" data-name="{{ $category->category_name }}" data-image="{{ asset($category->category_image) }}" data-round="{{ asset($category->round_cat_image) }}" id="{{ $category->category_id }}" class="edit_category_btn_ajax" data-toggle="modal" data-target="#exampleModal_edit_category"><i class="fa fa-pencil"></i></a></td>
                                                    <td><a href="#" style="color: #717171" data-toggle="modal" data-target="#exampleModal_confirm" class="delete_category_ajax" id="{{ $category->category_id }}"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->
                       <div class="pagination"> {{ $categories->links("pagination::bootstrap-4") }}</div>
                        @else
                         <div class="alert-danger p-3 text-center">There are no Categories Available</div>
                        @endif
                    </div> <!-- container-fluid -->

                </div> 
                <!-- content -->





<!-- categiry Modal add -->
<div class="modal fade" id="exampleModal_add_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Add category</h5>
        <button type="button" class="close category_modal_clode" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_category_form">
            <div class="modal-body">
                <div class="alert_input_0 text-danger"></div>
                <label for="label">Category</label>
                <input type="text" id="catgory_name_input" class="form-control" value="" placeholder="Enter category">
            </div>
            <div class="modal-body">
                <div class="alert_input_1 text-danger"></div>
                <label for="label">Category image</label>
                <input type="file" id="category_image_file" class="form-control">
            </div>

            <div class="modal-body">
                <div class="alert_input_2 text-danger"></div>
                <label for="label">Category Round image</label>
                <input type="file" id="category_round_image_file" class="form-control">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a  href="{{ url('/admin/add-category-ajax') }}"  class="btn btn-primary confirm_category_btn" id="">Submit</a>
            </div>
      </form>
    </div>
  </div>
</div>





<!-- categiry Modal edit -->
<div class="modal fade" id="exampleModal_edit_category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Edit category</h5>
        <button type="button" class="close category_modal_clode" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_category_form">
            <div class="modal-body">
                <div class="alert_edit_input_0 text-danger"></div>
                <label for="label">Category</label>
                <input type="text" id="category_edit_name_input" class="form-control" value="" placeholder="Enter category">
            </div>
            <div class="modal-body">
                <img src="" alt="" class="category_edit_image">
                <div class="alert_edit_input_1 text-danger"></div>
                <label for="label">Category image</label>
                <input type="file" id="category_edit_image_file" class="form-control">
            </div>

            <div class="modal-body">
                <img src="" alt="" class="category_edit_round_image">
                <div class="alert_edit_input_2 text-danger"></div>
                <label for="label">Category Round image</label>
                <input type="file" id="category_edit_round_image_file" class="form-control">
            </div>

        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a  href="{{ url('/admin/edit-category-ajax') }}"  class="btn btn-primary confirm_category_edit_submit" id="">Edit</a>
      </div>
      </form>
    </div>
  </div>
</div>









<!-- Category Modal Delete confirmation -->
<div class="modal fade" id="exampleModal_confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title text-center text-danger" id="">Do you wish to delete this category?</p>
        <button type="button" class="close category_modal_clode" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_category_form">
        <div class="modal-footer">
           
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a  href="{{ url('/admin/delete-category-ajax') }}"  class="btn btn-primary confirm_category_delete_submit" id="">Delete</a>
      </div>
      </form>
    </div>
  </div>
</div>






<!-- get category ajax url -->
  <a  href="{{ url('/admin/get-category-ajax') }}"  class="get_category_ajax_url" style="display: none;"></a>

<script>

$(document).ready(function(){
   
//    ADD CATEGORY
$(".confirm_category_btn").click(function(e){
    e.preventDefault();
    var category = $('#catgory_name_input').val();
    var url = $(this).attr('href');
    $(".alert_input_0").html('');
    $(".alert_input_1").html('');
    $(".alert_input_2").html('');

    var data = new FormData();
    var image = $('#category_image_file')[0].files[0];
    var round_image = $('#category_round_image_file')[0].files[0];
 

    data.append('image', image);
    data.append('category', category);
    data.append('round', round_image);

    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
            if(response.error){
                $(".alert_input_0").html(response.error.category);
                $(".alert_input_1").html(response.error.image);
                $(".alert_input_2").html(response.error.round);
            }else if(response.data){
                $('.category_modal_clode').click(); //close the modal
                $('.add_alert_category').show();
                $('.add_alert_category').html('The Category '+category+' has been added');
                get_category();
                remove_alert();
            }
        }
    });
});


// CSRF PAGE TOKEN
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}



function remove_alert()
{
    setTimeout(function(){
       $('.add_alert_category').html('');
       $('.add_alert_category').hide();
    }, 4000);
}



function get_category(){
    var url = $(".get_category_ajax_url").attr('href');

     csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: { 
            get_category: 'get_category'
        },
        success: function (response){
            $('.get_category_ajax').html(response);
        }
    });
}





// EDIT CATEGORY MODAL FIELD POPULATE
$(".get_category_ajax").on('click', '.edit_category_btn_ajax', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');
    var category = $(this).attr('data-name');
    var image = $(this).attr('data-image');
    var round = $(this).attr('data-round');

    $("#category_edit_name_input").val(category);
    $(".category_edit_image").attr('src', image);
    $(".category_edit_round_image").attr('src', round);
    $(".confirm_category_edit_submit").attr('id', id);
});




// UPDATE CATEGORY
$('.confirm_category_edit_submit').click(function(e){
    e.preventDefault();
    var category_name = $('#category_edit_name_input').val();
    var image = $('#category_edit_image_file')[0].files[0];
    var round_image = $('#category_edit_round_image_file')[0].files[0];
   
    var id = $(this).attr('id');
    var url = $(this).attr('href');
    empty_field() //empty input fields

    csrf_token()   // gets page csrf token

    var data = new FormData();
    data.append('image', image);
    data.append('category_id', id);
    data.append('category', category_name);
    data.append('round', round_image);

    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
           if(response.error){
               $(".alert_edit_input_0").html(response.error.category);
               $(".alert_edit_input_1").html(response.error.image);
               $(".alert_edit_input_2").html(response.error.round);
           }else if(response.data){
               $('.category_modal_clode').click(); //close the modal
               $(".add_alert_category").html('Category has been updated successfully!');
               $('.add_alert_category').show();
               get_category();
               remove_alert(); //remove success alert
           }
        }
    });
});


function empty_field(){
    $(".alert_edit_input_0").html('');
    $(".alert_edit_input_1").html('');
    $(".alert_edit_input_2").html('');
}




// CATEGORY FEATURE
$(".get_category_ajax").on('click', '.category_feature_ajax', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');

    csrf_token()   // gets page csrf token

     $.ajax({
        url: url,
        method: "post",
        data: {
            category_id: id
        },
        success: function (response){
            if(response.data){
                get_category();
            }
        }
    });
});





// GET DELETE CONFRIM MODAL
$(".get_category_ajax").on('click', '.delete_category_ajax', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $('.confirm_category_delete_submit').attr('id', id);
});


// DELETE CATEGORY 
$(".confirm_category_delete_submit").click(function(e){
   e.preventDefault();
   var id = $(this).attr('id');
   var url = $(this).attr('href');

    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            category_id: id
        },
        success: function (response){
            if(response.data)
            {
               $('.category_modal_clode').click(); //close the modal
               $(".add_alert_category").html('Category has been deleted successfully!');
               $('.add_alert_category').show();
               get_category();
               remove_alert(); //remove success alert
            }
        }
    });

});


// end 
});

</script>