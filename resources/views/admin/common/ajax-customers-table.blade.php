


<!-- ajax customer table start-->
<div class="table-responsive table-striped p-3">
    <h4 class="header-title mt-0 mb-1">Customer Table<a href="#" data-toggle="modal" data-target="#add_customer_modal" class="float-right btn btn-primary">Add customer</a></h4>
        <!-- <p class="sub-header"></p> -->
        <table id="basic-datatable" class="table">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Last login</th>
                    <th>Joined</th>
                    <th>Deactivate</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if(count($customers) != '')
                @foreach($customers as $customer)
                <tr>
                    <td>
                        @if($customer->user_image)
                        <img src="{{ asset($customer->user_image) }}" alt="{{ $customer->first_name }}" class="admin_image {{ $customer->active ? 'activate' : 'deactivate' }}">
                        @endif
                    </td>
                    <td>{{ $customer->first_name }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ date('d M Y', strtotime($customer->last_login)) }}</td>
                    <td>{{ date('d M Y', strtotime($customer->date_joined)) }}</td>
                    <td>
                        <div class="custom-control custom-checkbox mb-2">
                            <input type="checkbox" data-url="{{ url('/admin/deactivate-customer') }}" class="custom-control-input customer_deactivate_input" id="deactivate_{{ $customer->id }}" {{ $customer->is_deactivate ? 'checked' : ''}}>
                            <label class="custom-control-label" for="deactivate_{{ $customer->id }}"></label>
                        </div> 
                    </td>
                    <td>
                        <a href="{{ url('/admin/get-edit-customer-detail') }}" id="{{ $customer->id }}" data-image="{{ asset($customer->user_image) }}" class="icon edit_customer_modal_btn" data-toggle="modal" data-target="#edit_customer_modal"><i class="fa fa-pencil"></i></a>
                        <span style="padding: 0 10px;"></span>
                        <a href="#" id="{{ $customer->id }}" data-toggle="modal" data-target="#delete_customer_modal" class="icon delete_customer_modal"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div> <!-- end card body-->
    @if(count($customers) == '')
    <div class="p-3 text-center">There are no Customer Available</div>
    @endif
<!--ajax customer table end -->