

<!-- get category ajax table-->
<div class="table-responsive table-striped p-3">
    <h4 class="header-title mt-0 mb-1">Categories Table <a href="#" data-toggle="modal" data-target="#exampleModal_add_category" class="float-right btn btn-primary">Add Category</a></h4>
    <!-- <p class="sub-header"></p> -->
        <table id="basic-datatable" class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Image</th>
                <th>Featured</th>
                <th>Date Added</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
    
        <tbody>
            @foreach($categories as $category)
            <tr>
                <td><a href="#" style="color: #717171">{{ $category->category_name }}</a></td>
                <td><a href="#"><img src="{{ asset($category->category_image) }}" alt="image"></a></td>
                <td><a href="{{ url('/admin/category-feature') }}" id="{{ $category->category_id }}" class="category_feature_ajax"><i class="fa fa-{{ $category->is_feature ? 'check text-success' : 'times text-danger' }}"></i></a></td>
                <td>{{ date('d M Y', strtotime($category->date_added)) }}</td>
                <td><a href="{{ url('/admin/edit-category-ajax') }}" style="color: #717171" data-name="{{ $category->category_name }}" data-image="{{ asset($category->category_image) }}" data-round="{{ asset($category->round_cat_image) }}" id="{{ $category->category_id }}" class="edit_category_btn_ajax" data-toggle="modal" data-target="#exampleModal_edit_category"><i class="fa fa-pencil"></i></a></td>
                <td><a href="#" style="color: #717171" data-toggle="modal" data-target="#exampleModal_confirm" class="delete_category_ajax" id="{{ $category->category_id }}"><i class="fa fa-trash"></i></a></td>
            </tr>
            @endforeach
            </tbody>
    </table>
</div> <!-- end card body-->