




<div class="table-responsive table-striped p-3">
    <h4 class="header-title mt-0 mb-1">Brand Table <a href="#" data-toggle="modal" data-target="#exampleModal" class="float-right btn btn-primary">Add brand</a></h4>
    <!-- <p class="sub-header"></p> -->
        <table id="basic-datatable" class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Featured</th>
                <th>Date Added</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
            @foreach($brands as $brand)
            <tr>
                <td><a href="#" style="color: #717171" >{{ $brand->brand_name }}</a></td>
                <td>
                    <a href="{{ url('/admin/feature-brand-ajax') }}" style="color: #717171" id="{{ $brand->brand_id }}" class="feature_brand_ajax_btn">
                        <i class="fa fa-{{ $brand->is_feature ? 'check text-success' : 'times text-danger' }}"></i>
                    </a>
                </td>                                                    
                <td>{{ date('d M Y', strtotime($brand->brand_date_added)) }}</td>
                <td><a href="#" data-val="{{ $brand->brand_name }}" style="color: #717171" id="{{ $brand->brand_id }}" class="edit_brand_ajax_btn" data-toggle="modal" data-target="#exampleModal_edit"><i class="fa fa-pencil"></i></a></td>
                <td><a href="#" style="color: #717171" id="{{ $brand->brand_id }}" class="delete_brand_modal_dropdown" data-toggle="modal" data-target="#exampleModal_delete"><i class="fa fa-trash"></i></a></td>
            </tr>
            @endforeach
            </tbody>
    </table>
</div> 