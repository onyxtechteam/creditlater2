



<!-- customer image ajax-->
<div class="inner_image_container">
    <a href="{{ url('/admin/delete-customer-image-ajax') }}" id="{{ $customer->id }}" class="delete_customer_image">
        <i class="fa fa-trash" title="Delete image"></i>
    </a>
    <img src="{{ asset('admins/images/users/demo.png') }}" alt="" class="admin_image_modal active">
</div>