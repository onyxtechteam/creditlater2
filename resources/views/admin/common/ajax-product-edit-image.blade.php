



<!-- EDIT IMAGE AJAX-->
<div class="row">
@if($product->products_image)
    @php($images = explode(',', $product->products_image))
    @foreach($images as $key => $image)
    <div class="col-lg-3">
        <div class="modal-body image_modal_container">
            <img src="{{ asset($image) }}" alt="" class="edit_product_image">
            <div class="image_delete_icon">
                <a href="{{ url('/admin/delete-product-image-ajax') }}" id="{{ $product->id }}" data-key="{{ $key }}" class="product_image_trash"> <i class="fa fa-trash"></i></a>
            </div>
        </div>
    </div>
    @endforeach
@endif
</div> 