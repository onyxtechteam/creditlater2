

<!-- edit product ajax modal-->
<form action="" method="post" class="confirm_category_form">
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="modal-body">
                <div class="alert_input_0 text-danger"></div>
                <label for="label">Name:</label>
                <input type="text" id="edit_product_name_input" class="form-control" value="{{ $product->products_name }}" placeholder="Enter name">
            </div> 
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="modal-body">
                <label for="label">Brand:</label>
                <select id="edit_brand_input" class="form-control" required>
                    @if(count($brands) != '')
                        @foreach($brands as $brand)
                        <option value="{{ $brand->brand_id}}" {{ $brand->brand_id == $product->brand_id ? 'selected' : '' }}> {{ $brand->brand_name }}</option>
                        @endforeach
                    @endif
                </select>
            </div> 
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="modal-body">
                <label for="label">Categories:</label>
                <select id="edit_category_input" class="form-control" required>
                    @if(count($categories) != '')
                        @foreach($categories as $category)
                        <option value="{{ $category->category_id}}" {{ $category->category_id == $product->category_id ? 'selected' : '' }}> {{ $category->category_name }}</option>
                        @endforeach
                    @endif
                </select>
            </div> 
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <div class="alert_input_1 text-danger"></div>
                <label for="label">Price:</label>
                <input type="number" min="1" id="edit_price_input" class="form-control" value="{{ $product->products_price }}" placeholder="Price">
            </div> 
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <div class="alert_input_2 text-danger"></div>
                <label for="label">Price slash:</label>
                <input type="number" min="1" id="edit_price_slash_input" class="form-control" value="{{ $product->products_price_slash }}" placeholder="Price slash">
            </div> 
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <div class="alert_input_3 text-danger"></div>
                <label for="label">Quantity:</label>
                <input type="number" min="1" id="edit_quantity_input" class="form-control" value="{{ $product->products_quantity }}" placeholder="Quantity">
            </div> 
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <div class="alert_input_4 text-danger"></div>
                <label for="label">Initial payment:</label>
                <input type="number" min="1" id="edit_initial_input" class="form-control" value="{{ $product->initial_payment }}" placeholder="initial payment">
            </div> 
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <label for="label">Product type:</label>
                @if($product->products_type)
                   @php($type = explode(',', $product->products_type))
                   @php($small = in_array('S', $type) ? 'S' : '')
                   @php($medium = in_array('M', $type) ? 'M' : '')
                   @php($large = in_array('L', $type) ? 'L' : '')
                   @php($xl = in_array('XL', $type) ? 'XL' : '')
                @else
                   @php($small = '')
                   @php($medium = '')
                   @php($large = '')
                   @php($xl = '')
                @endif
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" id="edit_small_input" class="custom-control-input" value="{{ $small != '' ? 'S' : '' }}" {{ $small != '' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_small_input">Small</label>
                </div> 
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" id="edit_medium_input" class="custom-control-input" value="{{ $medium != '' ? 'M' : '' }}" {{ $medium != '' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_medium_input">Medium</label>
                </div> 
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" id="edit_large_input"  class="custom-control-input" value="{{ $large != '' ? 'L' : '' }}" {{ $large != '' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_large_input">Large</label>
                </div> 
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" id="edit_xtra_input"  class="custom-control-input" value="{{ $xl != '' ? 'XL' : '' }}" {{ $xl != '' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_xtra_input">Xtra large</label>
                </div> 
            </div> 
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <div class="alert_input_6 text-danger"></div>
                <label for="label">Shipping:</label>
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox"  class="custom-control-input edit_shipping_method_checkbox" value="" id="edit_free_shipping" {{ $product->shipping_method == 'free shipping' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_free_shipping">Free shipping</label>
                </div> 
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" class="custom-control-input edit_shipping_method_checkbox" value="" id="edit_local_pickup" {{ $product->shipping_method == 'local pickup' ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_local_pickup">Local pickup</label>
                </div> 
                <input type="hidden" value="{{ $product->shipping_method == 'free shipping' ? 'free shipping' : 'local pickup' }}" class="edit_shipping_method_input">
            </div> 
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <label for="label">Special:</label>
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" class="custom-control-input" value="{{ $product->is_special ? '1' : '1' }}" id="edit_special_input" {{ $product->is_special ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_special_input">Special</label>
                </div> 
            </div> 
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="modal-body">
                <label for="label">Feature:</label>
                <div class="custom-control custom-checkbox mb-2">
                    <input type="checkbox" class="custom-control-input" value="{{ $product->is_product_feature ? '1' : '' }}" id="edit_feature_input" {{ $product->is_product_feature ? 'checked' : '' }}>
                    <label class="custom-control-label" for="edit_feature_input">Feature</label>
                </div> 
            </div> 
        </div>

        <div class="col-lg-3">
            <div class="modal-body">
                <label for="label">Warranty:</label>
                <select id="edit_warranty_input" class="form-control">
                    <option value="">select</option>
                    @if($product->warranty)
                    <option value="{{ $product->warranty }}" {{ $product->warranty ? 'selected' : '' }}> {{ $product->warranty }}</option>
                    @endif
                    <option value="six months">Six months</option>
                    <option value="nine months">Nine months</option>
                    <option value="One year">One year</option>
                </select>
            </div> 
        </div>
        <div class="col-lg-3 col-sm-12">
            <div class="modal-body">
                <label for="label">Video link:</label>
                <input type="text" id="edit_video_link_input" class="form-control" value="{{ $product->products_video_link }}" placeholder="Video link">
            </div> 
        </div>
        <div class="col-lg-12 col-sm-12 edit_product_image_container">
            <div class="row">
                @if($product->products_image)
                    @php($images = explode(',', $product->products_image))
                    @foreach($images as $key => $image)
                    <div class="col-lg-3">
                        <div class="modal-body image_modal_container">
                            <img src="{{ asset($image) }}" alt="" class="edit_product_image">
                            <div class="image_delete_icon">
                                <a href="{{ url('/admin/delete-product-image-ajax') }}" id="{{ $product->id }}" data-key="{{ $key }}" class="product_image_trash"> <i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div> 
        </div>
        <div class="col-lg-3 col-sm-12">
            <div class="modal-body text-center">
                <div class="alert_input_5 text-danger"></div>
                <label for="label">Image:</label>
                <input type="file" id="edit_product_image_input" class="form-control" placeholder="Image" style="overflow: hidden;">
            </div> 
        </div>
        
        <div class="alert"></div> <!-- break point -->
        <div class="col-lg-12">
            <div class="modal-body">
                <div class="alert_input_7 text-danger"></div>
                <label for="label">Detail:</label>
                <textarea id="edit_detail_input" class="form-control" placeholder="Write something">{{ $product->products_detail }}</textarea>
                <script>
                        CKEDITOR.replace( 'edit_detail_input' );
                </script> 
            </div> 
        </div>
        <div class="col-lg-12">
            <div class="modal-body">
                <div class="alert_input_8 text-danger"></div>
                <label for="label">Description:</label>
                <textarea id="edit_description_input" class="form-control" placeholder="Write something">{{ $product->products_description }}</textarea>
                <script>
                        CKEDITOR.replace( 'edit_description_input' );
                </script> 
            </div> 
        </div>
        
        <div class="col-lg-12">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a  href="{{ url('/admin/edit-product-ajax') }}" id="{{ $product->id }}" class="btn btn-primary confirm_edit_product_btn" id="">Submit</a>
            </div> 
        </div>
    </div> <!-- end row-->  
</form>
<!-- edit product ajax modal end-->










<!-- ajax image update url -->
<a href="{{ url('/admin/update-product-image') }}" id="{{ $product->id }}" class="update_product_image" style="display: none;"></a>


<!-- ajax image total url -->
<a href="{{ url('/admin/get-total-product-image') }}" id="{{ $product->id }}" class="get_total_product_image" style="display: none;"></a>




















<script>

$(document).ready(function(){

function get_shipping(){


    
// ===========================================
// CONFIGURE FREE SHIPPING
// ===========================================
    var shipping = $(".edit_shipping_method_checkbox");
    $.each(shipping, function(index, current){
        $(current).click(function(){
            for(var i = 0; i < shipping.length; i++){
                if(index == i){
                    $(this).prop('checked');
                }else{
                    $(shipping[i]).prop('checked', false);
                }
            }
        });
    });


    // GET CHECKBOX FREE SHIPPING
    $("#edit_free_shipping").click(function(){
        $(this).val('');
        $('.edit_shipping_method_input').val('');
        if($(this).prop('checked')){
            $('.edit_shipping_method_input').val('free shipping');
        }
    });

    // GET CHECKBOX LOCAL PICKUP
    $("#edit_local_pickup").click(function(){
        $(this).val('');
        $('.edit_shipping_method_input').val('');
        if($(this).prop('checked')){
            $('.edit_shipping_method_input').val('local pickup');
        }
    });
}
get_shipping();





       
// =========================================== 
//  ADD PRODUCT IMAGES
// ===========================================
$('#edit_form_container').on('change', '#edit_product_image_input', function(e){
    var url = $(".update_product_image").attr('href');
    var id = $('.update_product_image').attr('id');
    $(".alert_input_5").html('');

    var data = new FormData();
    var image = $('#edit_product_image_input')[0].files[0];
    data.append('image', image);
    data.append('product_id', id);

    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
           if(response.error){
               $(".alert_input_5").html(response.error.image)
           }else if(response.data){
               get_total_image();
           }
        }
    });

});





// ===========================================================
// GET TOTAL IMAGE AND POPULATE EDIT MODAL IMAGE
// ===========================================================
function get_total_image()
{
    var url = $('.get_total_product_image').attr('href');
    var id = $('.get_total_product_image').attr('id');

    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id
        },
        success: function (response){
            $('.edit_product_image_container').html(response);
        }
    });
}



// ==========================================
// CSRF PAGE TOKEN
// ==========================================
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}






// =========================================== 
//  DELETE IMAGES FROM EDIT MODAL
// ===========================================
$('#edit_form_container').on('click', '.product_image_trash', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');
    var key = $(this).attr('data-key');

     $(".alert_input_5").html('');

    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id,
            key: key
        },
        success: function (response){
            if(response.error)
            {
                $(".alert_input_5").html(response.error.image);
            }else if(response.data){
                get_total_image();
            }
        }
    });
});




// =============================================
// GET PRODUCT TYPE(SIZE)
// =============================================

function get_sizes(){
    $("#edit_small_input").val('S');


    // GET CHECK BOX VALUE SMALL
    $("#edit_small_input").click(function(){
        $(this).val('');
        if($(this).prop('checked')){
            $(this).val('S');
        }
    });
    // GET CHECK BOX VALUE SMALL
    $("#edit_medium_input").click(function(){
        $(this).val('');
        if($(this).prop('checked')){
            $(this).val('M');
        }
    });
    // GET CHECK BOX VALUE SMALL
    $("#edit_large_input").click(function(){
        $(this).val('');
        if($(this).prop('checked')){
            $(this).val('L');
        }
    });
    // GET CHECK BOX VALUE SMALL
    $("#edit_xtra_input").click(function(){
        $(this).val('');
        if($(this).prop('checked')){
            $(this).val('XL');
        }
    });

}
get_sizes();



// GET CHECKBOX SPECIAL VALUE
$("#edit_special_input").click(function(){
    $(this).val('');
    if($(this).prop('checked')){
        $(this).val('1');
    }
});

// GET CHECKBOX FEATURE VALUE
$("#edit_feature_input").click(function(){
    $(this).val('');
    if($(this).prop('checked')){
        $(this).val('1');
    }
});





// ==============================================
// UPDATE PRODUCT
// ==============================================
$('#edit_form_container').on('click', '.confirm_edit_product_btn', function(e){ 
    e.preventDefault();
    var url = $(this).attr('href');
    var id = $(this).attr('id');
    update_product(url, id); // add prooduct method
});



function update_product(url, id){
    var name = $("#edit_product_name_input").val();
    var brand = $("#edit_brand_input").val();
    var category = $("#edit_category_input").val();
    var price = $("#edit_price_input").val();
    var price_slash = $("#edit_price_slash_input").val();
    var quantity = $("#edit_quantity_input").val();
    var initial = $("#edit_initial_input").val();
    var video_link = $("#edit_video_link_input").val();
    var small = $("#edit_small_input").val();
    var medium = $("#edit_medium_input").val();
    var large = $("#edit_large_input").val();
    var xl = $("#edit_xtra_input").val();
    var shipping = $(".edit_shipping_method_input").val();   
    var warranty = $("#edit_warranty_input").val();
    var special = $("#edit_special_input").val();
    var feature = $("#edit_feature_input").val();
    var detail = $("#edit_detail_input").val();
    var description = $("#edit_description_input").val();

    var data = new FormData();

    data.append('name', name);
    data.append('product_id', id);
    data.append('brand', brand);
    data.append('category', category);
    data.append('price', price);
    data.append('price_slash', price_slash);
    data.append('quantity', quantity);
    data.append('initial', initial);
    data.append('video_link', video_link);
    data.append('small', small);
    data.append('medium', medium);
    data.append('large', large);
    data.append('xl', xl);
    data.append('shipping', shipping);
    data.append('warranty', warranty);
    data.append('special', special);
    data.append('feature', feature);
    data.append('detail', detail);
    data.append('description', description);

    csrf_token()   // gets page csrf token
    reset_alert() // reset error 


    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
            if(response.error){
                asign_alert(response.error);
           }else if(response.data)
           {
               $(".confirm_category_form").parent().parent().find('#product_edit_modal_close_btn').click();
           }
        }
    });
}


function reset_alert(){
    $(".alert_input_0").html('');
    $(".alert_input_1").html('');
    $(".alert_input_2").html('');
    $(".alert_input_3").html('');
    $(".alert_input_4").html('');
    $(".alert_input_5").html('');
    $(".alert_input_6").html('');
    $(".alert_input_7").html('');
    $(".alert_input_8").html('');
}



function asign_alert(error){
    $(".alert_input_0").html(error.name);
    $(".alert_input_1").html(error.price);
    $(".alert_input_2").html(error.price_slash);
    $(".alert_input_3").html(error.quantity);
    $(".alert_input_4").html(error.initial);
    $(".alert_input_5").html(error.image);
    $(".alert_input_6").html(error.shipping);
    $(".alert_input_7").html(error.detail);
    $(".alert_input_8").html(error.description);
}





});
</script>