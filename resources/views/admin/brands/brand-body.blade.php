


            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                    
                    <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row page-title">
                            <div class="col-md-12">
                                <nav aria-label="breadcrumb" class="float-right mt-1">
                                    <ol class="breadcrumb">
                                       <!-- <form action="" method="">
                                           <div class="row">
                                           <div class="col-lg-12">
                                                <input type="text" id="brand" name="brand" class="form-control" placeholder="Enter brand">
                                            </div>
                                           </div>
                                       </form> -->
                                    </ol>
                                </nav>
                                <h4 class="mb-1 mt-0">Brand</h4>
                            </div>
                        </div>
                        @if($brands)
                        <div class="row">
                            <div class="col-12">
                                <div class="brand_alert_success alert-success p-3 mb-2" style="display: none;"></div>
                                <div class="card ajax_brand_call">
                                    <div class="table-responsive table-striped p-3">
                                        <h4 class="header-title mt-0 mb-1">Brand Table <a href="#" data-toggle="modal" data-target="#exampleModal" class="float-right btn btn-primary">Add brand</a></h4>
                                        <!-- <p class="sub-header"></p> -->
                                            <table id="basic-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Featured</th>
                                                    <th>Date Added</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                        
                                            <tbody>
                                                @foreach($brands as $brand)
                                                <tr>
                                                    <td><a href="#" style="color: #717171" >{{ $brand->brand_name }}</a></td>
                                                    <td>
                                                        <a href="{{ url('/admin/feature-brand-ajax') }}" style="color: #717171" id="{{ $brand->brand_id }}" class="feature_brand_ajax_btn">
                                                           <i class="fa fa-{{ $brand->is_feature ? 'check text-success' : 'times text-danger' }}"></i>
                                                        </a>
                                                    </td>                                                    
                                                    <td>{{ date('d M Y', strtotime($brand->brand_date_added)) }}</td>
                                                    <td><a href="#" data-val="{{ $brand->brand_name }}" style="color: #717171" id="{{ $brand->brand_id }}" class="edit_brand_ajax_btn" data-toggle="modal" data-target="#exampleModal_edit"><i class="fa fa-pencil"></i></a></td>
                                                    <td><a href="#" style="color: #717171" id="{{ $brand->brand_id }}" class="delete_brand_modal_dropdown" data-toggle="modal" data-target="#exampleModal_delete"><i class="fa fa-trash"></i></a></td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <div class="pagination"> {{ $brands->links("pagination::bootstrap-4") }}</div>
                        <!-- end row-->
                        @else
                         <div class="alert-danger p-3 text-center">There are no Brands Available</div>
                        @endif
                    </div> <!-- container-fluid -->

                </div> <!-- content -->


<a href="{{ url('/admin/get-brand-ajax') }}" class="ajax_get_brand_url"></a>






<!-- Brand Modal dropdown -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Add Brand</h5>
        <button type="button" class="close brand_modal_clode" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <div class="text-danger brand-edit-error"></div>
              <div class="alert_brand_error text-danger"></div>
              <label for="label">Brand</label>
              <input type="text" id="add_brand_input" class="form-control"value="" placeholder="Enter brand">
        
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="{{ url('/admin/add-brand') }}"  class="btn btn-primary confirm_add_brand_btn" id="">Add brand</a>
      </div>
      </form>
    </div>
  </div>
</div>






<!-- Brand Edit Modal dropdown -->
<div class="modal fade" id="exampleModal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Edit Brand</h5>
        <button type="button" class="close brand_modal_clode" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post">
      <div class="modal-body">
        <div class="text-danger brand-edit-error"></div>
              <div class="alert_brand_error text-danger"></div>
              <label for="label">Brand</label>
              <input type="text" id="edit_brand_input" class="form-control"value="" placeholder="Enter brand">
        
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="{{ url('/admin/edit-brand-ajax') }}"  class="btn btn-primary confirm_edit_brand_btn" id="">Edit brand</a>
      </div>
      </form>
    </div>
  </div>
</div>





<!-- Brand Modal Delete confirmation -->
<div class="modal fade" id="exampleModal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title text-center text-danger" id="">Do you wish to delete this Brand?</p>
        <button type="button" class="close brand_modal_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_brand_form">
        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a  href="{{ url('/admin/delete-brand-ajax') }}"  class="btn btn-primary confirm_brand_delete_submit" id="">Delete</a>
      </div>
      </form>
    </div>
  </div>
</div>








<script>
$(document).ready(function(){
  
//   ADD BRAND AJAX
var brandBtn = $(".confirm_add_brand_btn");
$(brandBtn).click(function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    var brand = $('#add_brand_input').val();
    $(".alert_brand_error").html('');
    $('.brand_alert_success').html('');

     csrf_token() //csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            brand: brand
        },
        success: function (response){
            if(response.error)
            {
                $(".alert_brand_error").html(response.error.brand);
            }else if(response.data){
                $(".brand_modal_clode").click(); //close the modal
                get_brand(); 
                $('.brand_alert_success').show();
                $('.brand_alert_success').html('The brand '+brand+' has been added successfully');
                remove_success_alert()// close success alert
            }
        }
    });

});



// GET BRAND AJAX
function get_brand()
{
    var url = $(".ajax_get_brand_url").attr('href');

    csrf_token() //csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            brand: 'brad'
        },
        success: function (response){
           $('.ajax_brand_call').html(response);
        }
    });
}



function remove_success_alert()
{
    setTimeout(function(){
        $('.brand_alert_success').html('');
        $('.brand_alert_success').hide();
    }, 4000);
}




// CSRF PAGE TOKEN
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}


// GET DELETE BRAND MODAL
$(".ajax_brand_call").on('click', '.delete_brand_modal_dropdown', function(e){
   e.preventDefault();
   var id = $(this).attr('id');
   $(".confirm_brand_delete_submit").attr('id', id);
});


// DELETE BRAND AJAX
$(".confirm_brand_delete_submit").click(function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');

    csrf_token() //csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            brand_id: id
        },
        success: function (response){
            if(response.data){
                $('.brand_alert_success').html('Brand has been deleted!');
                $('.brand_alert_success').show();
                $(".brand_modal_close").click();
                get_brand();
                remove_success_alert();
            }
        }
    });

});





// EDIT BRAND INPUT FIELD
$(".ajax_brand_call").on('click', '.edit_brand_ajax_btn', function(e){
    var brand_name = $(this).attr('data-val');
    var id = $(this).attr('id');
    $("#edit_brand_input").val(brand_name);
    $(".confirm_edit_brand_btn").attr('id', id);
});


// EDIT BRAND AJAX
$(".confirm_edit_brand_btn").click(function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');
    var brand = $("#edit_brand_input").val();

    csrf_token() //csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            brand_id: id,
            brand: brand
        },
        success: function (response){
           if(response.error)
           {
               $(".alert_brand_error").html(response.error.brand);
           }else if(response.data){
                $(".brand_modal_clode").click(); //close the modal
                get_brand(); 
                $('.brand_alert_success').show();
                $('.brand_alert_success').html('The brand '+brand+' has been updated successfully');
                remove_success_alert()// close success alert
            }
        }
    });
});





// FEATURE BRAND AJAX
$(".ajax_brand_call").on('click', '.feature_brand_ajax_btn', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');

    csrf_token() //csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            brand_id: id,
        },
        success: function (response){
            get_brand(); 
        }
    });
});



});
</script>