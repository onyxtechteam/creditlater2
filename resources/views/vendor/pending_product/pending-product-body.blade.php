


            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                    
                    <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row page-title">
                            <div class="col-md-12">
                                <nav aria-label="breadcrumb" class="float-right mt-1">
                                    <ol class="breadcrumb">
                                        <!-- some should be here -->
                                    </ol>
                                </nav>
                                <h4 class="mb-1 mt-0">Pending products</h4>
                            </div>
                        </div>
                       
                        <div class="alert alert-success text-center p-3 mb-2" style="display: none;" id="product_alert_success"></div>                        
                       
                       <!-- table start -->
                       <div class="get_products_table_container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="table-responsive p-3">
                                        <h4 class="header-title mt-0 mb-1">Product Table</h4>
                                        <!-- <p class="sub-header"></p> -->
                                            <table id="basic-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Image</th>
                                                    <th>Brand</th>
                                                    <th>Category</th>
                                                    <th>Product Price</th>
                                                    <th>Quantity</th>
                                                    <th>Sold</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        
                                            <tbody>
                                            @if($products)
                                                @foreach($products as $product)
                                                <tr>
                                                    <td><a href="#" style="color: #717171">{{ $product->products_name }}</a></td>
                                                    <td><a href="#"><img src="{{ asset(image($product->products_image, 0)) }}" alt="image" class="table-product-image"></a></td>
                                                    <td>{{ $product->brand_name }}</td>
                                                    <td>{{ $product->category_name }}</td>
                                                    <td>@money($product->products_price)</td>
                                                    <td>{{ $product->products_quantity }}</td>
                                                    <td>{{ $product->quantity_sold }}</td>
                                                    <td>
                                                        <a href="{{ url('/vendor/get-edit-product-ajax') }}" style="color: #717171"  id="{{ $product->id }}" class="product_edit_btn" data-toggle="modal" data-target="#edit_product_modal"><i class="fa fa-edit"></i></a>
                                                        <span style="padding: 0px 10px;"></span>
                                                        <a href="#" style="color: #717171" id="{{ $product->id }}" class="product_delete_btn" data-toggle="modal" data-target="#exampleModal_product_delete"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            @endif
                                        </table>

                                        @if(!$products)
                                        <div class="p-3 text-center">There are no Products Available</div>
                                        @endif
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->
                    </div>
                    @if($products)
                    <div class="pagination"> {{ $products->links("pagination::bootstrap-4") }}</div>
                    @endif
                    </div> <!-- container-fluid -->

                </div> <!-- content -->






<!-- EDIT PRODUCT MODAL-->
<div class="modal fade" id="edit_product_modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Edit product</h5>
                <button type="button" class="close" data-dismiss="modal" id="product_edit_modal_close_btn" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             
            <!-- preloader -->
            <div class="content_preloader">
                <img src="{{ asset('admins/images/ajax-loader.gif') }}" alt="">
            </div>
            <!-- end preloader -->

            <div id="edit_form_container"></div> <!--add edit modal form using ajax-->
                                                       
        </div>
    </div>
</div>
<!-- EDNTI PRODUCT MODAL END-->





<!-- DELETE PRODUCT CONFIRM MODAL-->
<div class="modal fade" id="exampleModal_product_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title text-center text-danger" id="">Do you wish to delete this Product?</p>
        <button type="button" class="close product_modal_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_brand_form">
        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a  href="{{ url('/vendor/delete-pending-product-ajax') }}"  class="btn btn-primary confirm_product_delete_submit" id="">Delete</a>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- DELETE PRODUCT CONFIRM MODAL END-->





 <a  href="{{ url('/vendor/get-all-pending-product-ajax') }}" class="get_all_products" style="display: none;"></a> <!-- ajax url for get all product-->






<script>
$(document).ready(function(){




// ================================================
// DELETE PRODUCT
// ================================================
$('.get_products_table_container').on('click', '.product_delete_btn', function(e){  
    e.preventDefault()
    var id = $(this).attr('id');
    $('.confirm_product_delete_submit').attr('id', id);
});


$('.confirm_product_delete_submit').click(function(e){
    e.preventDefault()
    var id = $(this).attr('id');
    var url = $(this).attr('href');

     csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id,
        },
        success: function (response){
            if(response.data)
            {
                get_all_products();
                $('#product_alert_success').html('Product has been deleted successfully!');
                $('#product_alert_success').show();
                $(".product_modal_close").click();
                remove_success_alert();
            }
        }
    });
});





   // CSRF PAGE TOKEN
   function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}




// remove preloader from edit modal
function remove_preloader()
{
    setTimeout(function(){
        $(".content_preloader").hide();
    }, 1500);
}




function remove_success_alert(){
    setTimeout(function(){
       $("#product_alert_success").hide();
    }, 4000);
}





// ======================================================
// GET ALL PRODUCTS
// ======================================================
function get_all_products(){
    var url = $(".get_all_products").attr('href');
    
    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            get_products: 'get_products'
        },
        success: function (response){
            $('.get_products_table_container').html(response);
        }
    });
}








// ================================================
// POPULATE EDIT MODAL BOX
// ================================================
$('.get_products_table_container').on('click', '.product_edit_btn', function(e){ 
    e.preventDefault();
    var url = $(this).attr('href');
    var id = $(this).attr('id');

    $(".content_preloader").show();
    remove_preloader();

     csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id,
        },
        success: function (response){
            $('#edit_form_container').html(response);
        }
    });
    
});










































// end
});
</script>