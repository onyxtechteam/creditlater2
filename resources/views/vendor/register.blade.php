@extends("vendor.layout")


<!-- navigations -->
@section("navigations")
    @include("vendor.header.navigation")
@endsection

<!-- page content-->
@section("content")
    @include("vendor.register.register-body")
@endsection