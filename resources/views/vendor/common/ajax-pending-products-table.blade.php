


 <!-- table start -->
 <div class="get_products_table_container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="table-responsive p-3">
                                        <h4 class="header-title mt-0 mb-1">Product Table</h4>
                                        <!-- <p class="sub-header"></p> -->
                                            <table id="basic-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Image</th>
                                                    <th>Brand</th>
                                                    <th>Category</th>
                                                    <th>Product Price</th>
                                                    <th>Quantity</th>
                                                    <th>Sold</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        
                                            <tbody>
                                            @if($products)
                                                @foreach($products as $product)
                                                <tr>
                                                    <td><a href="#" style="color: #717171">{{ $product->products_name }}</a></td>
                                                    <td><a href="#"><img src="{{ asset(image($product->products_image, 0)) }}" alt="image" class="table-product-image"></a></td>
                                                    <td>{{ $product->brand_name }}</td>
                                                    <td>{{ $product->category_name }}</td>
                                                    <td>@money($product->products_price)</td>
                                                    <td>{{ $product->products_quantity }}</td>
                                                    <td>{{ $product->quantity_sold }}</td>
                                                    <td>
                                                        <a href="{{ url('/vendor/get-edit-product-ajax') }}" style="color: #717171"  id="{{ $product->id }}" class="product_edit_btn" data-toggle="modal" data-target="#edit_product_modal"><i class="fa fa-edit"></i></a>
                                                        <span style="padding: 0px 10px;"></span>
                                                        <a href="#" style="color: #717171" id="{{ $product->id }}" class="product_delete_btn" data-toggle="modal" data-target="#exampleModal_product_delete"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            @endif
                                        </table>

                                        @if(!$products)
                                        <div class="p-3 text-center">There are no Products Available</div>
                                        @endif
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->
                    </div>