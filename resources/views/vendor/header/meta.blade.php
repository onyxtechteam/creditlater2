

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="OnyxDS" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <!-- App favicon -->
        <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

        <!-- Summernote css -->
        <link href="{{ asset('admins/libs/summernote/summernote-bs4.css') }}" rel="stylesheet" />

        <!-- plugins -->
        <link href="{{ asset('admins/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{ asset('admins/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admins/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admins/css/app.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('web/css/font-awesome.min.css') }}">

        <!-- main style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/main-style.css') }}">


        <!-- latest jquery-->
        <script src="{{ asset('vendors/js/jquery-3.3.1.min.js') }}"></script>

        <!-- ck editor -->
        <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

        