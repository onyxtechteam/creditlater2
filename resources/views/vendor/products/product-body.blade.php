


            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                    
                    <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row page-title">
                            <div class="col-md-12">
                                <nav aria-label="breadcrumb" class="float-right mt-1">
                                    <ol class="breadcrumb">
                                        <a href="#" data-toggle="modal" data-target="#add_product_modal" class="float-right btn btn-primary">Add product</a>
                                    </ol>
                                </nav>
                                <h4 class="mb-1 mt-0">Products</h4>
                            </div>
                        </div>
                       
                        <div class="alert alert-success text-center p-3 mb-2" style="display: none;" id="product_alert_success"></div>                        
                       
                       <!-- table start -->
                       <div class="get_products_table_container">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="table-responsive p-3">
                                        <h4 class="header-title mt-0 mb-1">Product Table</h4>
                                        <!-- <p class="sub-header"></p> -->
                                            <table id="basic-datatable" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Image</th>
                                                    <th>Brand</th>
                                                    <th>Category</th>
                                                    <th>Product Price</th>
                                                    <th>Featured</th>
                                                    <th>Quantity</th>
                                                    <th>Sold</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        
                                            <tbody>
                                            @if($products)
                                                @foreach($products as $product)
                                                <tr>
                                                    <td><a href="#" style="color: #717171">{{ $product->products_name }}</a></td>
                                                    <td><a href="#"><img src="{{ asset(image($product->products_image, 0)) }}" alt="image" class="table-product-image"></a></td>
                                                    <td>{{ $product->brand_name }}</td>
                                                    <td>{{ $product->category_name }}</td>
                                                    <td>@money($product->products_price)</td>
                                                    <td>
                                                        <div class="custom-control custom-checkbox mb-2">
                                                            <input type="checkbox" data-url="{{ url('/vendor/get-feature-product-ajax') }}" class="custom-control-input product_feature_input" id="feature_{{ $product->id }}" {{ $product->is_product_feature ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="feature_{{ $product->id }}"></label>
                                                        </div>   
                                                    </td>
                                                    <td>{{ $product->products_quantity }}</td>
                                                    <td>{{ $product->quantity_sold }}</td>
                                                    <td>
                                                        <a href="{{ url('/vendor/get-edit-product-ajax') }}" style="color: #717171"  id="{{ $product->id }}" class="product_edit_btn" data-toggle="modal" data-target="#edit_product_modal"><i class="fa fa-edit"></i></a>
                                                        <span style="padding: 0px 10px;"></span>
                                                        <a href="#" style="color: #717171" id="{{ $product->id }}" class="product_delete_btn" data-toggle="modal" data-target="#exampleModal_product_delete"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            @endif
                                        </table>
                                
                                        @if(!$products)
                                        <div class="p-3 text-center">There are no Products Available</div>
                                        @endif
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->
                    </div>
                    @if($products)
                    <div class="pagination"> {{ $products->links("pagination::bootstrap-4") }}</div>
                    @endif
                    </div> <!-- container-fluid -->

                </div> <!-- content -->
















<!-- ADD PRODUCT MODAL -->

<div class="modal fade" id="add_product_modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Add product</h5>
                <button type="button" class="close" data-dismiss="modal" id="product_modal_close_btn" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" class="confirm_category_form">
               <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="modal-body">
                            <div class="alert_input_0 text-danger"></div>
                            <label for="label">Name:</label>
                            <input type="text" id="product_name_input" class="form-control" value="" placeholder="Enter name">
                        </div> 
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="modal-body">
                            <label for="label">Brand:</label>
                            <select id="brand_input" class="form-control" required>
                                @if(count($brands) != '')
                                    @foreach($brands as $brand)
                                    <option value="{{ $brand->brand_id}}">{{ $brand->brand_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> 
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="modal-body">
                            <label for="label">Categories:</label>
                            <select id="category_input" class="form-control" required>
                                @if(count($categories) != '')
                                    @foreach($categories as $category)
                                    <option value="{{ $category->category_id}}">{{ $category->category_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <div class="alert_input_1 text-danger"></div>
                            <label for="label">Price:</label>
                            <input type="number" min="1" id="price_input" class="form-control" value="" placeholder="Price">
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <div class="alert_input_2 text-danger"></div>
                            <label for="label">Price slash:</label>
                            <input type="number" min="1" id="price_slash_input" class="form-control" value="" placeholder="Price slash">
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <div class="alert_input_3 text-danger"></div>
                            <label for="label">Quantity:</label>
                            <input type="number" min="1" id="quantity_input" class="form-control" value="" placeholder="Quantity">
                        </div> 
                    </div>
                  
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <div class="alert_input_4 text-danger"></div>
                            <label for="label">Initial payment:</label>
                            <input type="number" min="1" id="initial_input" class="form-control" value="" placeholder="initial payment">
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <label for="label">Video link:</label>
                            <input type="text" id="video_link_input" class="form-control" value="" placeholder="Video link">
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-12">
                        <div class="modal-body">
                            <div class="alert_input_5 text-danger"></div>
                            <label for="label">Image:</label>
                            <input type="file" id="product_image_input" class="form-control" placeholder="Image" style="overflow: hidden;">
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <label for="label">Product type:</label>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" id="small_input" class="custom-control-input" value="" checked>
                                <label class="custom-control-label" for="small_input">Small</label>
                            </div> 
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" id="medium_input" class="custom-control-input" value="">
                                <label class="custom-control-label" for="medium_input">Medium</label>
                            </div> 
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" id="large_input"  class="custom-control-input" value="">
                                <label class="custom-control-label" for="large_input">Large</label>
                            </div> 
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" id="xtra_input"  class="custom-control-input" value="">
                                <label class="custom-control-label" for="xtra_input">Xtra large</label>
                            </div> 
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <div class="alert_input_6 text-danger"></div>
                            <label for="label">Shipping:</label>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox"  class="custom-control-input shipping_method_input" value="" id="free_shipping" checked>
                                <label class="custom-control-label" for="free_shipping">Free shipping</label>
                            </div> 
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input shipping_method_input" value="" id="local_pickup">
                                <label class="custom-control-label" for="local_pickup">Local pickup</label>
                            </div> 
                            <input type="hidden" value="" class="_shipping_method_input">
                        </div> 
                    </div>
                    <div class="col-lg-3">
                        <div class="modal-body">
                            <label for="label">Warranty:</label>
                            <select id="warranty_input" class="form-control">
                                <option value="">select</option>
                                <option value="six months">Six months</option>
                                <option value="nine months">Nine months</option>
                                <option value="One year">One year</option>
                            </select>
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <label for="label">Special:</label>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" value="" id="special_input">
                                <label class="custom-control-label" for="special_input">Special</label>
                            </div> 
                        </div> 
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="modal-body">
                            <label for="label">Feature:</label>
                            <div class="custom-control custom-checkbox mb-2">
                                <input type="checkbox" class="custom-control-input" value="" id="feature_input" checked>
                                <label class="custom-control-label" for="feature_input">Feature</label>
                            </div> 
                        </div> 
                    </div>
                    <div class="alert"></div> <!-- break point -->
                    <div class="col-lg-12">
                        <div class="modal-body">
                            <div class="alert_input_7 text-danger"></div>
                            <label for="label">Detail:</label>
                            <textarea id="detail_input" class="form-control" placeholder="Write something"></textarea>
                            <script>
                                    CKEDITOR.replace( 'detail_input' );
                            </script> 
                        </div> 
                    </div>
                    <div class="col-lg-12">
                        <div class="modal-body">
                            <div class="alert_input_8 text-danger"></div>
                            <label for="label">Description:</label>
                            <textarea id="description_input" class="form-control" placeholder="Write something"></textarea>
                            <script>
                                    CKEDITOR.replace( 'description_input' );
                            </script> 
                        </div> 
                    </div>
                    
                    <div class="col-lg-12">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a  href="{{ url('/vendor/add-product-ajax') }}"  class="btn btn-primary confirm_add_product_btn" id="">Submit</a>
                        </div> 
                    </div>
               </div> <!-- end row-->  
            </form>                                           
        </div>
    </div>
</div>
<!-- ADD PRODUCT MODAL END-->






<!-- DELETE PRODUCT CONFIRM MODAL-->
<div class="modal fade" id="exampleModal_product_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="modal-title text-center text-danger" id="">Do you wish to delete this Product?</p>
        <button type="button" class="close product_modal_close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="post" class="confirm_brand_form">
        <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <a  href="{{ url('/vendor/delete-product-ajax') }}"  class="btn btn-primary confirm_product_delete_submit" id="">Delete</a>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- DELETE PRODUCT CONFIRM MODAL END-->




<!-- EDIT PRODUCT MODAL-->
<div class="modal fade" id="edit_product_modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Edit product</h5>
                <button type="button" class="close" data-dismiss="modal" id="product_edit_modal_close_btn" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             
            <!-- preloader -->
            <div class="content_preloader">
                <img src="{{ asset('admins/images/ajax-loader.gif') }}" alt="">
            </div>
            <!-- end preloader -->

            <div id="edit_form_container"></div> <!--add edit modal form using ajax-->
                                                       
        </div>
    </div>
</div>
<!-- EDNTI PRODUCT MODAL END-->






 <a  href="{{ url('/vendor/get-all-product-ajax') }}" class="get_all_products" style="display: none;"></a> <!-- ajax url for get all product-->












<script>
$(document).ready(function(){
   

function get_sizes(){
$("#small_input").val('S');

// GET CHECK BOX VALUE SMALL
$("#small_input").click(function(){
    $(this).val('');
    if($(this).prop('checked')){
        $(this).val('S');
    }
});
// GET CHECK BOX VALUE SMALL
$("#medium_input").click(function(){
    $(this).val('');
    if($(this).prop('checked')){
        $(this).val('M');
    }
});
// GET CHECK BOX VALUE SMALL
$("#large_input").click(function(){
    $(this).val('');
    if($(this).prop('checked')){
        $(this).val('L');
    }
});
// GET CHECK BOX VALUE SMALL
$("#xtra_input").click(function(){
    $(this).val('');
    if($(this).prop('checked')){
        $(this).val('XL');
    }
});

}
get_sizes();





function get_shipping(){
    // CONFIGURE FREE SHIPPING
    var shipping = $(".shipping_method_input");
    $.each(shipping, function(index, current){
        $(current).click(function(){
            for(var i = 0; i < shipping.length; i++){
                if(index == i){
                    $(this).prop('checked');
                }else{
                    $(shipping[i]).prop('checked', false);
                }
            }
        });
    });


    // GET CHECKBOX FREE SHIPPING
    $("._shipping_method_input").val('free shipping');
    $("#free_shipping").click(function(){
        $(this).val('');
        $('._shipping_method_input').val('');
        if($(this).prop('checked')){
            $('._shipping_method_input').val('free shipping');
        }
    });

    // GET CHECKBOX LOCAL PICKUP
    $("#local_pickup").click(function(){
        $(this).val('');
        $('._shipping_method_input').val('');
        if($(this).prop('checked')){
            $('._shipping_method_input').val('local pickup');
        }
    });
}
get_shipping();







function special_feature(){
    // GET CHECKBOX SPECIAL VALUE
    $("#special_input").click(function(){
        $(this).val('');
        if($(this).prop('checked')){
            $(this).val('1');
        }
    });

    // GET CHECKBOX FEATURE VALUE
    $("#feature_input").val('1');
    $("#feature_input").click(function(){
        $(this).val('');
        if($(this).prop('checked')){
            $(this).val('1');
        }
    });
}
special_feature();






// =======================================================
// ADD PRODUCT
// =======================================================
$('.confirm_add_product_btn').click(function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    add_product(url); // add prooduct method
});




function add_product(url){
    var name = $("#product_name_input").val();
    var brand = $("#brand_input").val();
    var category = $("#category_input").val();
    var price = $("#price_input").val();
    var price_slash = $("#price_slash_input").val();
    var quantity = $("#quantity_input").val();
    var initial = $("#initial_input").val();
    var video_link = $("#video_link_input").val();
    var image = $("#product_image_input");
    var small = $("#small_input").val();
    var medium = $("#medium_input").val();
    var large = $("#large_input").val();
    var xl = $("#xtra_input").val();
    var shipping = $("._shipping_method_input").val();   
    var warranty = $("#warranty_input").val();
    var special = $("#special_input").val();
    var feature = $("#feature_input").val();
    var detail = $("#detail_input").val();
    var description = $("#description_input").val();

    reset_alert() //resets alert

    var data = new FormData();
    var image = $(image)[0].files[0];


    data.append('image', image);
    data.append('name', name);
    data.append('brand', brand);
    data.append('category', category);
    data.append('price', price);
    data.append('price_slash', price_slash);
    data.append('quantity', quantity);
    data.append('initial', initial);
    data.append('video_link', video_link);
    data.append('small', small);
    data.append('medium', medium);
    data.append('large', large);
    data.append('xl', xl);
    data.append('shipping', shipping);
    data.append('warranty', warranty);
    data.append('special', special);
    data.append('feature', feature);
    data.append('detail', detail);
    data.append('description', description);

    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
           if(response.error){
                asign_alert(response.error);
           }else if(response.data){
               get_all_products(); //get all products
               $('#product_alert_success').html('Product has been added successfully!');
               $('#product_alert_success').show();
               $('#product_modal_close_btn').click();
               remove_success_alert();
           }
        }
    });
}

   // CSRF PAGE TOKEN
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}






function reset_alert(){
    $(".alert_input_0").html('');
    $(".alert_input_1").html('');
    $(".alert_input_2").html('');
    $(".alert_input_3").html('');
    $(".alert_input_4").html('');
    $(".alert_input_5").html('');
    $(".alert_input_6").html('');
    $(".alert_input_7").html('');
    $(".alert_input_8").html('');
}



function asign_alert(error){
    $(".alert_input_0").html(error.name);
    $(".alert_input_1").html(error.price);
    $(".alert_input_2").html(error.price_slash);
    $(".alert_input_3").html(error.quantity);
    $(".alert_input_4").html(error.initial);
    $(".alert_input_5").html(error.image);
    $(".alert_input_6").html(error.shipping);
    $(".alert_input_7").html(error.detail);
    $(".alert_input_8").html(error.description);
}




// REMOVE ALERT SUCCESS
function remove_success_alert(){
    setTimeout(function(){
       $("#product_alert_success").hide();
    }, 4000);
}






// remove preloader from edit modal
function remove_preloader()
{
    setTimeout(function(){
        $(".content_preloader").hide();
    }, 1500);
}




// ======================================================
// GET ALL PRODUCTS
// ======================================================
function get_all_products(){
    var url = $(".get_all_products").attr('href');
    
    csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            get_products: 'get_products'
        },
        success: function (response){
            $('.get_products_table_container').html(response);
        }
    });
}







// ==================================================
// PRODUCT FEATURE 
// ==================================================
$('.get_products_table_container').on('click', '.product_feature_input', function(e){
    var id = $(this).attr('id');
    var url = $(this).attr('data-url');

     csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id,
        },
        success: function (response){
            if(response.data)
            {
                get_all_products();
            }
        }
    });
});







// ================================================
// DELETE PRODUCT
// ================================================
$('.get_products_table_container').on('click', '.product_delete_btn', function(e){  
    e.preventDefault()
    var id = $(this).attr('id');
    $('.confirm_product_delete_submit').attr('id', id);
});


$('.confirm_product_delete_submit').click(function(e){
    e.preventDefault()
    var id = $(this).attr('id');
    var url = $(this).attr('href');

     csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id,
        },
        success: function (response){
            if(response.data)
            {
                get_all_products();
                $('#product_alert_success').html('Product has been deleted successfully!');
                $('#product_alert_success').show();
                $(".product_modal_close").click();
                remove_success_alert();
            }
        }
    });
});





// ================================================
// POPULATE EDIT MODAL BOX
// ================================================
$('.get_products_table_container').on('click', '.product_edit_btn', function(e){ 
    e.preventDefault();
    var url = $(this).attr('href');
    var id = $(this).attr('id');

    $(".content_preloader").show();
    remove_preloader();

     csrf_token()   // gets page csrf token

    $.ajax({
        url: url,
        method: "post",
        data: {
            product_id: id,
        },
        success: function (response){
            $('#edit_form_container').html(response);
        }
    });
    
});




// =======================================================
// GET ALL PRODUCT AFTER UPDATE
// =======================================================
$("#product_edit_modal_close_btn").click(function(){
    if($(this).data('clicked', true)){
        get_all_products(); //get all products after update has been made
    }
});






//    end
});
</script>