@extends("admin.layout")


<!-- navigations -->
@section("navigations")
    @include("vendor.header.navigation")
@endsection

<!-- page content-->
@section("content")
    @include("vendor.login.login-body")
@endsection