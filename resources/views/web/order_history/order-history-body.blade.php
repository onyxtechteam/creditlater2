





<section>
    <div class="container">
    @if(count($buyer_orders) != '')
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Reference</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Initial Payment</th>
                    <th scope="col">Payment type</th>
                    <th scope="col">Date</th>
                    <th scope="col">View</th>
                    </tr>
                </thead>
                <tbody>
                @php($x = 1)
                    @foreach($buyer_orders as $order)
                    <tr>
                        <th scope="row">{{ $x }}</th>
                        <td>{{ $order->reference }}</td>
                        <td>@money($order->amount)</td>
                        <td>@money($order->initial_payment)</td>
                        <td>{{ $order->payment_type }}</td>
                        <td>{{ date('d M Y', strtotime($order->paid_date))}}</td>
                        <td><a href="{{ url('/order-detail/'.$order->buyer_token) }}" class="text-warning">view more</a></td>
                    </tr>
                    @php($x++)
                @endforeach
                </tbody>
                </table>
            </div> 
        @else
        <!-- section start -->
        <section class="p-0 bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="error-section empty-shopping-cart">
                            <h2>You hav no order</h2>
                            <a href="{{ url('/products') }}" class="btn btn-normal">continue shopping</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- Section ends -->
        @endif 
    </div>
</section>
