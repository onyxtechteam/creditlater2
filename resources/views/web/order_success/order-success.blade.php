
<!-- thank-you section start -->
<section class="section-big-py-space light-layout">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="success-text"><i class="fa fa-check-circle" aria-hidden="true"></i>
                    <h2>thank you</h2>
                    <p>Payment is successfully processsed and your order is on the way</p>
                    <a href="{{ url('/') }}" class="btn  btn-normal">Back to home</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->
