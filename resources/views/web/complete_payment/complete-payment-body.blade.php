
<!-- breadcrumb start -->
<div class="breadcrumb-main ">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="breadcrumb-contain">
                    <div>
                        <h2>Complete Payment</h2>
                        <ul>
                            <li><a href="#">home</a></li>
                            <li><i class="fa fa-angle-double-right"></i></li>
                            <li><a href="#">Complete Payment</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb End -->


<!--section start-->
<section class="login-page section-big-py-space bg-light">
    <div class="custom-container">
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-8 offset-xl-4 offset-lg-3 offset-md-2">
                <div class="theme-card">
                    <h3 class="text-center">payment Form</h3>
                    @if(Session::has('success'))
                    <div class="alert-success p-3 mb-3 text-center">{{ Session::get('success') }}</div>
                    @endif
                    <form action="{{ url('/complete-payment') }}" method="post" class="theme-form">
                        <div class="form-group">
                            @if($errors->first('email'))
                                <div class="text-danger">{{ $errors->first('email')}}</div>
                            @endif
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            @if($errors->first('amount'))
                                <div class="text-danger">{{ $errors->first('amount')}}</div>
                            @endif
                            <label for="review">Amount</label>
                            <input type="number"  min="1" class="form-control" name="amount" placeholder="Amount" required>
                        </div>
                       
                        <button class="btn btn-normal" id="complete_installment_payment_btn">Pay now</button>
                        <a class="float-right txt-default mt-2" href="{{ url('/installment-orders/complete-payment') }}" id="fgpwd"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i> Back</a>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Section ends-->






