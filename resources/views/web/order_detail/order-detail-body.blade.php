
@if($order_detail)
<!-- section start -->
<section class="section-big-py-space bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="account-sidebar"><a class="popup-btn">My Order</a></div>
                <div class="dashboard-left">
                    <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> back</span></div>
                    <div class="block-content ">
                        <ul>
                            <li class="active"><a href="{{ url('/account') }}">My Account</a></li>
                            <li><a href="{{ url('/wishlist') }}">My Wishlist</a></li>
                            <li><a href="{{ url('/cart') }}">My Cart</a></li>
                            <li><a href="#">Newsletter</a></li>
                            <li><a href="{{ url('/order-history') }}">My Orders</a></li>
                            <li><a href="{{ url('/return-history') }}">Return history</a></li>
                            <li><a href="{{ url('/verification') }}">Verification</a></li>
                            <li><a href="{{ url('/change-password') }}">Change Password</a></li>
                            <li class="last"><a href="{{ url('/logout') }}">Log Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="dashboard-right">
                    <div class="dashboard">
                        <div class="page-title">
                        @if(Session::has('success'))
                        <div class="alert-success p-3 text-center mb-3">{{ Session::get('success') }}</div>
                        @endif
                       
                            <h2>My Order</h2></div>
                        <div class="welcome-msg">
                            <p>Hello,  {{ $order_detail->first_name }} {{ $order_detail->last_name }}</p>
                            <p> This is the list of the order you made on {{ date('d M Y', strtotime($order_detail->paid_date)) }}<br></p>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="box col-lg-6">
                                <div class="box-title">
                                    <h3>Order number</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h6>{{ $order_detail->reference }}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="box col-lg-6">
                                <div class="box-title">
                                    <h3>Order Date</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h6>{{ date('d M Y', strtotime($order_detail->paid_date)) }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box col-lg-4">
                                <div class="box-title">
                                    <h3>Total</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h6>@money($order_detail->amount)</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="box col-lg-4">
                                <div class="box-title">
                                    <h3>Balance</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h6>@money($order_detail->buyer_balance)</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="box col-lg-4">
                                <div class="box-title">
                                    <h3>Payment Type</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h6>{{ $order_detail->payment_type }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="box-account box-info">
                            <div class="box-head">
                                <h2>Shipping  Information</h2></div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="box">
                                        <div class="box-title">
                                            <h3>Contact Information</h3></div>
                                        <div class="box-content">
                                            <h6> Name: {{ $order_detail->first_name }} {{ $order_detail->last_name }}</h6>
                                            <h6> Email: {{ $order_detail->email }}</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="box">
                                        <div class="box-title">
                                            <h3>Transactions</h3><a href="#" data-toggle="modal" class="quick-view-btn" data-target="#quick-view-transactions" title="Quick View">Check transaction</a>
                                        </div>
                                        <div class="box-content">
                                            <p>Check all transactions under this section</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="box">
                                    <div class="box-title">
                                        <h3>Shipping Address</h3></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h6>Billing Address</h6><address>{{ $order_detail->address }}</address></div>
                                        <div class="col-sm-6">
                                            <h6>City: {{ $order_detail->city }}</h6>
                                            <h6>State: {{ $order_detail->state }}</h6>
                                            <h6>Country: {{ $order_detail->country }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                               
                            </div>
                             <div class="complete-payment text-right">
                                @if(!$order_detail->is_completed)
                                  <b><a href="#" data-toggle="modal" class="quick-view-btn btn-normal btn" data-target="#quick-complete-transactions">Complete payment</a></b>
                                @endif
                             </div>
                            </div>
                        </div>
                    </div>

<br><br>
                    <div class="dashboard-right">
                        <div class="dashboard">
                            <div class="box">
                                <div class="box-title">
                                    <h3>Products</h3></div>
                                
                                    @if(count($paid_products) != '')
                                    <div class="row">
                                    @foreach($paid_products as $product)
                                    @if($product->quantity != $product->product_return_qty)
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-lg-3 col-sm-3 col-4">
                                                    <ul>
                                                        <li><img src="{{ asset(image($product->products_image, 0)) }}" alt="" style="width: 70px; height: 80px;"></li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-6 col-sm-5 col-6">
                                                    <div class="row">
                                                         <div class="col-lg-6">
                                                            <p>Name: {{ $product->products_name }}</p>
                                                            <p>Price: @money( $product->products_price )</p>
                                                            <p>Quantity: {{ $product->quantity }}</p>
                                                         </div>
                                                         <div class="col-lg-6">
                                                            <p>Size: {{ $product->size == 'unspecified' ? '-' : $product->size }}</p>
                                                            <p>Total: @money($product->total)</p>
                                                         </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-sm-4 col-12">
                                                    <ul>
                                                        <li>
                                                            <a href="{{ url('/order-return/') }}" data-toggle="modal" id="{{ $product->id }}" data-token="{{ $product->paids_token }}"class="btn btn-normal drop_down_modal_open btn drop_down_modal_open_btn" data-target="#return_product_modal">Return</a>
                                                        </li>
                                                    </ul>                                                
                                                </div>
                                            </div>
                                            <br>
                                        </div> 
                                        @endif
                                        @endforeach
                                    </div>
                                        @else
                                    <div class="p-3 text-center">There are no product!</div>
                                    @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- section end -->
@else
<div class="p-3 text-center">There are no orders yet!</div>
@endif










<!-- Quick-view modal popup start-->
<div class="modal fade bd-example-modal-lg theme-modal" id="quick-view-transactions" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content quick-view-modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row" id="quick_view_dropdown_modal">
                    <div class="col-lg-12">
                        <div class="tm-img">
                            <img src="{{ asset('admins/images/users/2.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="theader">
                            <div class="box-t">
                                <h3>Transactions</h3>
                            </div>
                            <div class="box-content">
                                <p>Check all transactions under this section</p>
                                @if(!$order_detail->is_completed)
                                 <p class="text-danger">Not completed</p>
                                @else
                                <p class="text-success">completed</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="t-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Transaction ID</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Paid</th>
                                    <th scope="col">Balance</th>
                                    <th scope="col">Date</th>
                                    </tr>
                                </thead>
                                @if(count($transactions) != '')
                                <tbody>
                                @php($x = 1)
                                    @foreach($transactions as $transaction)
                                    <tr>
                                        <th scope="row">{{ $x }}</th>
                                        <td>{{ $transaction->transction_reference}}</td>
                                        <td>@money($transaction->total)</td>
                                        <td>@money($transaction->paid)</td>
                                        <td>@money($transaction->balance)</td>
                                        <td>{{ date('d M Y', strtotime($transaction->last_paid_date)) }}</td>
                                    </tr>
                                    @php( $x++)
                                    @endforeach
                                </tbody>
                                @endif
                            </table>
                            @if(count($transactions) == '')
                                <div class="text-center p-3">There are no transactions</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="quick-view-preloader transaction-preloader">
                    <img src="{{ asset('web/images/ajax-loader.gif') }}" alt="ajax-loader">
                </div>
                <div class="product-buttons text-right"><a href="#" id="close" data-dismiss="modal" aria-label="Close" class="btn btn-normal">Close</a></div>
                
            </div>
        </div>
    </div>
</div>
<!-- Quick-view modal popup end-->





<!-- Quick-view modal popup start-->
<div class="modal fade bd-example-modal-lg theme-modal" id="quick-complete-transactions" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content quick-view-modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row" id="quick_view_dropdown_modal">
                    <div class="col-lg-12">
                        <div class="tm-img">
                            <img src="{{ asset('admins/images/users/2.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="theader">
                            <div class="box-t">
                                <h3>Transactions</h3>
                            </div>
                            <div class="box-content">
                                <p>Complete transaction</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="complet-form-container">
                            <form action="{{ url('/complete-payment') }}" method="post">
                                <div class="form-group">
                                    <div class="t-alert alert_0 text-danger"></div>
                                    <label for="">Email: </label>
                                    <input type="text" name="email" id="email_input" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <div class="t-alert alert_1 text-danger"></div>
                                    <label for="">Password: </label>
                                    <input type="password" name="password" id="password_input" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <div class="t-alert alert_2 text-danger"></div>
                                    <label for="">Amount: </label>
                                    <input type="number" min='1'name="amount" id="amount_input" class="form-control" value="">
                                </div>
                                <input type="hidden" class="buyer_token" name="buyer_token"value="{{ $order_detail->buyer_token }}">
                                <button type="submit" class="complete_installment_payment" style="display: none;"></button>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="quick-view-preloader transaction-preloader">
                    <img src="{{ asset('web/images/ajax-loader.gif') }}" alt="ajax-loader">
                </div>
                <div class="product-buttons text-right">
                    <a href="#" id="close" data-dismiss="modal" aria-label="Close" class="btn btn-normal">Close</a>
                    <a href="{{ url('/complete-installment-payment-ajax') }}" id="complete_installment_payment" class="btn btn-normal">Pay now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Quick-view modal popup end-->






<!-- RETURN a product modal popup start-->
<div class="modal fade bd-example-modal-lg theme-modal" id="return_product_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content quick-view-modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row" id="quick_view_dropdown_modal">
                    <div class="col-lg-12">
                        <div class="tm-img">
                            @if($user->user_image)
                                <img src="{{ asset($user->user_image)}}" alt="">
                            @else
                            <img src="{{ asset('admins/images/users/demo.png')}}" alt="">
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="theader">
                            <div class="box-t">
                                <h3>Return Order</h3>
                            </div>
                            <div class="box-content">
                                <p>Return product order</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="complet-form-container">
                            <form action="{{ url('/complete-payment') }}" method="post" enctype="multipart/form-data">
                               <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="r-alert alert_0 text-danger"></div>
                                            <label for="">Email: </label>
                                            <input type="text"  id="return_email_input" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="r-alert alert_1 text-danger"></div>
                                            <label for="">Quantity: </label>
                                            <input type="number"  min="1" id="return_quantity_input" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="r-alert alert_2 text-danger"></div>
                                            <label for="">Warranty slip: </label>
                                            <input type="file"  id="return_warranty_input" class="form-control" style="overflow: hidden;">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="r-alert alert_3 text-danger"></div>
                                            <label for="">Message: </label>
                                            <textarea id="return_mesage_input" class="form-control" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>
                               </div>
                                 <input type="hidden" class="return_product_id" value="">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="quick-view-preloader transaction-preloader">
                    <img src="{{ asset('web/images/ajax-loader.gif') }}" alt="ajax-loader">
                </div>
                <div class="product-buttons text-right">
                    <a href="#" id="close" data-dismiss="modal" aria-label="Close" class="btn btn-normal">Close</a>
                    <a href="{{ url('/return-product-ajax') }}" data-token="{{ $order_detail->buyer_token }}" id="return_product_btn" class="btn btn-normal">Return Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Quick-view modal popup end-->




<script>

$(document).ready(function(){
   
   
// show and hide preloader
$(".drop_down_modal_open").click(function(e){
    e.preventDefault();
    preloader();
});

// preloader hides after one minute
function preloader(){
    setTimeout(function(){
        $(".quick-view-preloader").hide();
    }, 1500);
    $(".quick-view-preloader").show();
}





//=====================================
// COMPLETE INSTALLLMENT PAYMENT
// ===================================
$("#complete_installment_payment").click(function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    complete_payment(url);
});



function complete_payment(url){
    var email = $('#email_input').val();
    var password = $('#password_input').val();
    var amount = $('#amount_input').val();
    var buyer_token = $(".buyer_token").val();
    $(".t-alert").html('')

    csrf_token()   // gets page csrf token
    
    $.ajax({
        url: url,
        method: "post",
        data: {
           email: email,
           password: password,
           amount: amount,
           buyer_token: buyer_token
        },
        success: function (response){
           if(response.error)
           {
                get_error(response.error);
           }else if(response.data){
               $(".complete_installment_payment").click();
           }
        }
    });
}


// CSRF PAGE TOKEN
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}



function get_error(error){
    $('.alert_0').html(error.email);
    $('.alert_1').html(error.password);
    $('.alert_2').html(error.amount);
}


// assign id to product return dropdown input field
$(".drop_down_modal_open_btn").click(function(){
    var id = $(this).attr('id');
    var token = $(this).attr('data-token');
    $(".return_product_id").val(id);
});


// ================================================
// RETURN PRODUCT NOW
// ================================================
$("#return_product_btn").click(function(e){
    e.preventDefault();
    $(".r-alert").html('')
    var token = $(this).attr('data-token');
    var url = $(this).attr('href');
    var product_id = $(".return_product_id").val();
    var email = $('#return_email_input').val();
    var quantity = $("#return_quantity_input").val();
    var message = $("#return_mesage_input").val();
    var warranty = $("#return_warranty_input");

    csrf_token()   // gets page csrf token

    var data = new FormData();
    var warranty = $(warranty)[0].files[0];

    data.append('email', email);
    data.append('product_id', product_id);
    data.append('token', token);
    data.append('quantity', quantity);
    data.append('message', message);
    data.append('warranty', warranty);
    
    $.ajax({
        url: url,
        method: "post",
        data: data,
        contentType: false,
        processData: false,
        success: function (response){
           if(response.error){
               get_error(response.error);
           }else if(response.data){
                location.reload();
           }
        }
    });

     
});




function get_error(error){
    $(".alert_0").html(error.email);
    $(".alert_1").html(error.quantity);
    $(".alert_2").html(error.warranty);
    $(".alert_3").html(error.message);
}




});

</script>