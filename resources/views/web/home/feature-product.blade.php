
<div class="collection-content col">
    <div class="page-main-content">
        <div class="collection-product-wrapper">
            <div class="product-wrapper-grid">
                @if(count($products) != '')
                <div class="row">
                <!-- content start -->
                @foreach($products as $product)
                @php( $image = explode(',', $product->products_image))
                    <div class="col-lg-3 col-6 col-grid-box">
                        <div class="product">
                            <div class="product-box">
                                <div class="product-imgbox">
                                <a href="{{ url('/detail/'.$product->id)}}">
                                    @if(isset($image[0]) && $image[0] != '')
                                    <div class="product-front">
                                        <img src="{{ asset($image[0]) }}" class="img-fluid   " alt="product">
                                    </div>
                                    @endif
                                    @if(isset($image[1]) && $image[1] != '')
                                    <div class="product-back">
                                        <img src="{{ asset($image[1]) }}" class="img-fluid   " alt="product">
                                    </div>
                                    @endif
                                </a>
                                </div>
                                <div class="product-detail detail-center ">
                                    <div class="detail-title">
                                        <div class="detail-left">
                                            <div class="rating-star">
                                            @if(star_rating($product->id))
                                            @for($i = 1; $i <= 5; $i++)
                                                @if($i <= star_rating($product->id))
                                                <i class="fa fa-star text-warning"></i>
                                                @else
                                                <i class="fa fa-star text-secondary"></i>
                                                @endif
                                            @endfor
                                            @else
                                            @for($i = 1; $i <= 5; $i++)
                                                <i class="fa fa-star text-secondary"></i>
                                            @endfor
                                            @endif
                                            </div>
                                            <a href="#">
                                                <h6 class="price-title">
                                                {{ $product->products_name}}
                                                </h6>
                                            </a>
                                        </div>
                                        <div class="detail-right">
                                            <div class="check-price">
                                            @money($product->products_price_slash)
                                            </div>
                                            <div class="price">
                                                <div class="price">
                                                @money($product->products_price)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="icon-detail">
                                        <button class="quick-add-to-cartBtn" id="{{ $product->id }}" data-url="{{ url('/quick-add-to-cart') }}" data-toggle="modal" data-target="#addtocart" title="Add to cart">
                                            <i class="fa fa-shopping-cart" ></i>
                                        </button>
                                        @if(Session::has('user'))
                                        <a href="javascript:void(0)" id="{{ $product->id }}" data-url="{{ url('/quick-add-to-wishlist') }}" class="quick-add-to-wishlist" title="Add to Wishlist">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </a>
                                        @endif
                                        <a href="{{ url('/quick-view') }}" class="quick-view-btn" id="{{ $product->id }}" data-toggle="modal" data-target="#quick-view" title="Quick View">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                    <!-- content end -->
                </div>
                @else
                   <div class="text-center p-3">There are no products yet</div>
                @endif
            </div>
        </div>
    </div>
</div>

<br><br>