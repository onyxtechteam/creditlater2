@extends("web.layout")

@section("title")
    Registration success
@endsection

<!-- about page contents-->
@section("content")
    @include("web.thankyou.registration-success")
@endsection