

<!-- section start -->
<section class="section-big-py-space bg-light">
    <div class="product_return_container">
        <div class="row">
            <div class="col-lg-3">
                <div class="account-sidebar"><a class="popup-btn">Product return</a></div>
                <div class="dashboard-left">
                    <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> back</span></div>
                    <div class="block-content ">
                        <ul>
                            <li class="active"><a href="{{ url('/account') }}">My Account</a></li>
                            <li><a href="{{ url('/wishlist') }}">My Wishlist</a></li>
                            <li><a href="{{ url('/cart') }}">My Cart</a></li>
                            <li><a href="#">Newsletter</a></li>
                            <li><a href="{{ url('/order-history') }}">My Orders</a></li>
                            <li><a href="{{ url('/return-history') }}">Return history</a></li>
                            <li><a href="{{ url('/verification') }}">Verification</a></li>
                            <li><a href="{{ url('/change-password') }}">Change Password</a></li>
                            <li class="last"><a href="{{ url('/logout') }}">Log Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                    <div class="dashboard-right">
                        <div class="dashboard">
                            <div class="box">
                                <div class="box-title">
                                    <h3>Products return</h3></div>
                                    @if(count($return_histories) != '')
                                    <div class="row">
                                        @foreach($return_histories as $return_history)
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-lg-2 col-sm-2 col-4">
                                                    <ul>
                                                        <li>
                                                           <a href="#"><img src="{{ asset($return_history->warranty_slip) }}" alt="" style="width: 70px; height: 80px;"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-10 col-sm-10 col-10">
                                                    <div class="row">
                                                         <div class="col-lg-4 col-sm-4">
                                                            <p>Reference: {{ $return_history->w_buyer_token  }}</p>
                                                            <p>Quantity: {{ $return_history->w_quantity }}</p>
                                                         </div>
                                                         <div class="col-lg-8 col-sm-8">
                                                            <p>Date: {{ date('d M Y', strtotime($return_history->return_date)) }}<span class="float-right"><a href="#" data-toggle="modal" class="quick-view-btn btn-normal btn" data-target="#return_view_product_modal">View detail</a></span></p>
                                                            <p>Total:  @money($return_history->w_total_price)</p>
                                                            <p>Email: {{ $return_history->w_email }}</p>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                            @endforeach
                                        </div> 
                                    @else
                                  
                                    <div class="p-3 text-center">There are no product returns yet!</div>
                                    @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- section end -->

<div class="p-3 text-center">There are no orders yet!</div>








<!-- RETURN a product modal popup start-->
<div class="modal fade bd-example-modal-lg theme-modal" id="return_view_product_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content quick-view-modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row" id="quick_view_dropdown_modal">
                    <div class="col-lg-12">
                        <div class="tm-img">
                        @if($user->user_image)
                            <img src="{{ asset($user->user_image)}}" alt="">
                        @else
                        <img src="{{ asset('admins/images/users/demo.png')}}" alt="">
                        @endif
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="theader">
                            <div class="box-t">
                                <h3>Return Order</h3>
                            </div>
                            <div class="box-content">
                                <p>Return product order</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="complet-form-container">
                            <form action="{{ url('/complete-payment') }}" method="post" enctype="multipart/form-data">
                               <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="r-alert alert_0 text-danger"></div>
                                            <label for="">Email: </label>
                                            <input type="text"  id="return_email_input" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="r-alert alert_1 text-danger"></div>
                                            <label for="">Quantity: </label>
                                            <input type="number"  min="1" id="return_quantity_input" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="r-alert alert_2 text-danger"></div>
                                            <label for="">Warranty slip: </label>
                                            <input type="file"  id="return_warranty_input" class="form-control" style="overflow: hidden;">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="r-alert alert_3 text-danger"></div>
                                            <label for="">Message: </label>
                                            <textarea id="return_mesage_input" class="form-control" cols="30" rows="5"></textarea>
                                        </div>
                                    </div>
                               </div>
                                 <input type="hidden" class="return_product_id" value="">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="quick-view-preloader transaction-preloader">
                    <img src="{{ asset('web/images/ajax-loader.gif') }}" alt="ajax-loader">
                </div>
                <div class="product-buttons text-right">
                    <a href="#" id="close" data-dismiss="modal" aria-label="Close" class="btn btn-normal">Close</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Quick-view modal popup end-->


