

@if(Session::has('cart'))


<!-- section start -->
<section class="section-big-py-space bg-light">
    <div class="custom-container">
        <div class="checkout-page contact-page">
            <div class="checkout-form">
                <form action="{{ url('/checkout') }}" method="post" class="checkout_form">
                    <div class="row">
                    
                        <div class="col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-title">
                                <h3>Billing Details</h3>
                            </div>
                            <div class="theme-form">
                                <div class="row check-out ">
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="alert-field alert_checkout_0 text-danger"></div>
                                        <label>First Name</label>
                                        <input type="text" id="checkout_first_name" value="" placeholder="First name">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="alert-field alert_checkout_1 text-danger"></div>
                                        <label>Last Name</label>
                                        <input type="text" id="checkout_last_name"  value="" placeholder="Last name">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="alert-field alert_checkout_2 text-danger"></div>
                                        <label class="field-label">Phone</label>
                                        <input type="text" id="checkout_phone" value="" placeholder="Phone">
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="alert-field alert_checkout_3 text-danger"></div>
                                        <label class="field-label">Email Address</label>
                                        <input type="text" id="checkout_email" value="" placeholder="Email">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert-field alert_checkout_4 text-danger"></div>
                                        <label class="field-label">Address</label>
                                        <input type="text" id="checkout_address" value="" placeholder="Street address">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert-field alert_checkout_5 text-danger"></div>
                                        <label class="field-label">Town/City</label>
                                        <input type="text" id="checkout_city" value="" placeholder="City">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert-field alert_checkout_6 text-danger"></div>
                                        <label class="field-label">State</label>
                                        <input type="text" id="checkout_state" value="" placeholder="State">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                        <div class="alert-field alert_checkout_7 text-danger"></div>
                                        <label class="field-label">County</label>
                                        <input type="text" id="checkout_country" value="" placeholder="">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                        <div class="alert-field alert_checkout_8 text-danger"></div>
                                        <label class="field-label">Postal Code</label>
                                        <input type="text" id="checkout_postal_code" value="" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-details theme-form  section-big-mt-space">
                                <div class="order-box">
                                    <div class="title-box">
                                        <div>Product <span>Total</span></div>
                                    </div>
                                    <ul class="qty">
                                        @foreach(Session::get('cart')->_items as $values)
                                        @php($cart_item = $values['product'])
                                        @php($total = $values['quantity'] * $cart_item['products_price'])
                                        <li>{{ $cart_item['products_name'] }} × {{ $values['quantity'] }} <span>@money($total) </span></li>
                                        <li><b>Shipping method:</b>  <span style="font-size: 15px;">{{ $cart_item['shipping_method'] }}</span></li>
                                        @endforeach
                                    </ul>
                                    @if(count($installmentPeriods) != '')
                                    <ul class="sub-total">
                                        <div class="alert_checkout_9 text-danger"></div>
                                        @foreach($installmentPeriods as $period)
                                        <li>
                                                <input type="checkbox" class="installment_checkbox_btn" data-url="{{ url('/get-installment-payment-period') }}" id="{{ $period->id }}" data-id="{{ $period->percentage == 0 ? 'pay_out_right' : '' }}" value="{{ $period->percentage }}" {{ $period->percentage == 0 ? 'checked' : ''}}>
                                            {{ $period->period }} <span class="count">+%{{ $period->percentage }}</span>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <input type="hidden" id="checkout_total_amount" class="" value="{{ Session::get('cart')->_totalPrice }}">
                                    <input type="hidden" id="checkout_initial_amount" class="" value="">
                                    <input type="hidden" id="checkout_payment_type" class="" value="">
                                    @endif

                                     <ul class="sub-total">
                                        <li>Total <span class="count"><b class="checkout_total_field">@money(Session::get('cart')->_totalPrice)</b></span></li>
                                    </ul>
                                    <ul class="sub-total checkout_initial_installment">
                                        <li>Initial installment <span class="count"><b class="checkout_initial_installment_field">@money(Session::get('cart')->_totalPrice)</b></span></li>
                                    </ul>
                                    <ul class="sub-total" id="verification_alert" style="display: none;">
                                        <div class="text-center text-danger alert-danger p-3"></div>
                                    </ul>
                                </div>
                                    <div class="text-right">
                                        <!-- <a href="{{ url('/installments') }}" class="btn-normal btn" id="pay_installment_btn" data-url="">Buy outright</a> -->
                                        <button type="submit" data-url="{{ url('/pay-now') }}" id="checkout_payment_btn" class="btn-normal btn">Buy now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</section>
<!-- section end -->
@endif





<script>
$(document).ready(function(){
    
// ==========================================
// GET INSTALLMENT PERIODS
// ==========================================
var initial = $(".checkout_initial_installment");
$(initial).hide();


function get_installment(){
var installments = $(".installment_checkbox_btn");
    $.each(installments, function(index, current){
        $(this).click(function(){
            for(var i = 0; i < installments.length; i++){
            if(index == i){
                $(this).prop('checked');
            }else{
                $(installments[i]).prop('checked', false);
            }
        }
        });
    });

   $(installments).click(function(e){
    $(initial).hide();
    $("#checkout_total_amount").val('');
    $("#checkout_initial_amount").val('');
    $('#checkout_payment_type').val('');

    var url = $(this).attr('data-url');
    var period_id = $(this).attr('id');

        if($(this).prop('checked'))
        {
            var percentage = $(this).val();
            $('#checkout_payment_type').val(period_id);
            $("#installment_checkbox").val(percentage);
            get_installments_price(url, period_id, percentage);
        }
   });
}
get_installment();




// ============================================
// GET INSTALLMENT AND MINIMUM PRICE
// ============================================
function get_installments_price(url, period_id, percentage){
    if(percentage != 0){
        $(initial).show();
    }

    csrf_token()   // gets page csrf token
    
    $.ajax({
        url: url,
        method: "post",
        data: {
            period_id: period_id
        },
        success: function (response){
            if(response.data){
                $("#checkout_total_amount").val(response.data.total);
                $(".checkout_total_field").html(response.data.total_format);
                $("#checkout_initial_amount").val(response.data.initial)
                $(".checkout_initial_installment_field").html(response.data.initial_installment);
                if(percentage == 0){
                    $("#checkout_initial_amount").val('')
                }
            }
        }
    });
}



// =============================================
// CHECKOUT PAYMENT
// =============================================
$("#checkout_payment_btn").click(function(e){
    e.preventDefault();
    var url = $(this).attr('data-url');
    buy_now(url);
});



function buy_now(url){
    var first_name = $("#checkout_first_name").val();
    var last_name = $("#checkout_last_name").val();
    var phone = $("#checkout_phone").val();
    var email = $("#checkout_email").val();
    var address = $("#checkout_address").val();
    var city = $("#checkout_city").val();
    var state = $("#checkout_state").val();
    var country = $("#checkout_country").val();
    var postal_code = $("#checkout_postal_code").val();
    var total = $("#checkout_total_amount").val();
    var initial = $("#checkout_initial_amount").val();
    var payment_type = $('#checkout_payment_type').val();
    
    $("#verification_alert").hide();
    alert_reset();

     csrf_token()   // gets page csrf token
    
    $.ajax({
        url: url,
        method: "post",
        data: {
            first_name: first_name,
            last_name: last_name,
            phone: phone,
            email: email,
            address: address,
            city: city,
            state: state,
            country: country,
            postal_code: postal_code,
            total: total,
            initial: initial,
            payment_type: payment_type
        },
        success: function (response){
            if(response.error){
                error_alert(response.error);
            }else if(response.verification){
                location.assign(response.verification);
            }else if(response.is_approved){
                $("#verification_alert").show();
                $("#verification_alert").children().html(response.is_approved);
            }else if(response.data){
                $(".checkout_form").submit();
                // console.log(response)
            }
        }
    });

}



function error_alert(error){
    $(".alert_checkout_0").html(error.first_name);
    $(".alert_checkout_1").html(error.last_name);
    $(".alert_checkout_2").html(error.phone);
    $(".alert_checkout_3").html(error.email);
    $(".alert_checkout_4").html(error.address);
    $(".alert_checkout_5").html(error.city);
    $(".alert_checkout_6").html(error.state);
    $(".alert_checkout_7").html(error.country);
    $(".alert_checkout_8").html(error.postal_code);
}



function alert_reset(){
    $(".alert-field").html('');
}



$('.installment_period').click(function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr('href');
    var quantity = $(".item_detail_quantity_btn").val();


    csrf_token()   // gets page csrf token
    
    $.ajax({
        url: url,
        method: "post",
        data: {
            period_id: id,
            product_id: product_id,
            quantity: quantity
        },
        success: function (response){
           $('.product_installment_price').html(response.data.total)
           $(".minimum_installment_price").html(response.data.installment)
        }
    });


});





// CSRF PAGE TOKEN
function csrf_token(){
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf_token']").attr("content")
        }
    });
}




















});
</script>

















