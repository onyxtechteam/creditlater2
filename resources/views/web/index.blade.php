@extends("web.layout")


@section("navigations")
    @include("web.header.top-navigation")
    @include("web.header.middle-navigation")
    @include("web.header.bottom-navigation")
@endsection

<!-- home page contents-->
@section("content")
    @include("web.home.slider")
    @include("web.home.banner")
    @include("web.home.collection-banner")
    @include("web.home.tab-category")
    @include("web.home.media-banner")
    @include("web.home.feature-product")
    @include("web.home.deal-banner")
@endsection