<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


use App\Models\Web\User;
use Session;
use DB;

class Auth extends Model
{   
    use HasFactory;

    public static function user($string = null)
    {
        if(Session::has('user'))
        {
            if(isset($string))
            {
                return Session::get('user')[$string];
            }
            return Session::get('user');
        }
        return false;
    }




    public static function login($email)
    {
        if($email)
        {
            $user = User::where('email', $email)->first();
            if($user)
            {
                $userDetails = ['id' => $user->id, 'first_name' => $user->first_name, 
                'last_name' => $user->last_name, 'email' => $user->email, 
                'token' => $user->token, 'active' => $user->active,
                'user_image' => $user->user_image
                 ];

                $user->active = 1;
                $user->save();
                Session::put('user', $userDetails);
                return true;
            }
        }
        return false;
    }




    public static function is_loggedin()
    {
        if(self::user('id'))
        {
            $user = DB::table('users')->where('id', self::user('id'))->where('email', self::user('email'))->first();
            if($user)
            {
                return true;
            }
        }
        return false;
    }


    public static function logout()
    {
        if(Session::has('user'))
        {
            Session::forget('user');
            return true;
        }
        return false;
    }


    // end
}

