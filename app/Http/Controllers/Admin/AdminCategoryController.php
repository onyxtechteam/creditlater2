<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Admin\Auth;
use App\Models\Admin\Admin;
use App\Models\Admin\Image;

use DB;
use Session;

class AdminCategoryController extends Controller
{
    public function index()
    {
        if(!Session::has('admin'))
        {
            return redirect('admin/login');
        }

        // get all categories
        $categories = DB::table("categories")->paginate(15);
        if(count($categories) == "")
        {
            $categories = null;
        }

        $admin = Admin::where('token',  Auth::admin()['token'])->where('id',  Auth::admin()['id'])->first();
       
        if(!$admin)
        {
            $admin = null;
        }


        return view('admin.category', compact('categories', 'admin'));
    }




    public function category_approve($id)
    {
        if($id)
        {
            $category = DB::table('categories')->where('category_id', $id)->first();
            if($category)
            {
                $is_approved = $category->is_approved ? 0 : 1;
                DB::table('categories')->where('category_id', $id)->update(array(
                      'is_approved' => $is_approved
                ));
            }
        }
        return back();
    }






    public function admin_add_category_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $error = null;

            if(empty($request->category))
            {
                $error['category'] = '*Category field is required';
            }else if(strlen($request->category) < 3)
            {
                $error['category'] = '*Category must be minimum of 3 characters';
            }else if(strlen($request->category) > 50)
            {
                $error['category'] = '*Category must be maximum of 50 characters';
            }

            if(isset($_FILES['image']))
            {
                $file = $_FILES['image'];
                $image = new Image();

                $fileExt = explode('.', $file['name']);
                $fileExt = end($fileExt);

                $categoryImage = 'category_'.uniqid().'.'.$fileExt;
                $image->resize_image($file, [ 'name' => $categoryImage, 'width' => 40, 'height' => 40, 'size_allowed' => 1000000,'file_destination' => 'admins/images/category_image/' ]);
                
                if(!$image->passed())
                {
                    $error['image'] = $image->error();
                }
            }else{
                $error['image'] = '*Category image field is required';
            }

            if(isset($_FILES['round']))
            {
                $file = $_FILES['round'];
                $image = new Image();

                $fileExt = explode('.', $file['name']);
                $fileExt = end($fileExt);

                $roundImage = 'round_'.uniqid().'.'.$fileExt;
                $image->resize_image($file, [ 'name' => $roundImage, 'width' => 108, 'height' => 108, 'size_allowed' => 1000000,'file_destination' => 'admins/images/round_category/' ]);
                
                if(!$image->passed())
                {
                    $error['round'] = $image->error();
                }
            }else{
                $error['round'] = '*Round image field is required';
            }
        

            if(!empty($error))
            {
                return response()->json(['error' => $error]);
            }else{
                $newCategory = DB::table('categories')->insert(array(
                   'category_name' => $request->category,
                   'category_image' => 'admins/images/category_image/'.$categoryImage,
                   'round_cat_image' => 'admins/images/round_category/'.$roundImage,
                ));
                if($newCategory)
                {
                    $data = true;
                }
            }
          
                    
        }
        return response()->json(['data' => $data]);
    }





    public function admin_get_category_ajax()
    {
        $categories = DB::table("categories")->get();
        if(count($categories) == "")
        {
            $categories = null;
        }

        return view('admin.common.ajax-category-table', compact('categories'));
    }




    // UPDATE CATEGORY
    public function admin_edit_category_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $error = null;

            if(empty($request->category))
            {
                $error['category'] = '*Category field is required';
            }else if(strlen($request->category) < 3)
            {
                $error['category'] = '*Category must be minimum of 3 characters';
            }else if(strlen($request->category) > 50)
            {
                $error['category'] = '*Category must be maximum of 50 characters';
            }else{
                $old_category = DB::table('categories')->where('category_name', $request->category)->where('category_id', $request->category_id)->first();
                if(!$old_category)
                {
                    $total_category = DB::table('categories')->where('category_name', $request->category)->get();
                    if(count($total_category) > 0)
                    {
                        $error['category'] = '*Category '.$request->category.' already exist';
                    }
                }
            }

            $old_category_image = DB::table('categories')->where('category_id', $request->category_id)->first();

            if(isset($_FILES['image']))
            {
                $file = $_FILES['image'];
                $image = new Image();

                $fileExt = explode('.', $file['name']);
                $fileExt = end($fileExt);

                $categoryImage = 'category_'.uniqid().'.'.$fileExt;
                $newCategoryImage = 'admins/images/category_image/'.$categoryImage;
                $image->resize_image($file, [ 'name' => $categoryImage, 'width' => 40, 'height' => 40, 'size_allowed' => 1000000,'file_destination' => 'admins/images/category_image/' ]);
                
                if(!$image->passed())
                {
                    $error['image'] = $image->error();
                }
                Image::delete($old_category_image->category_image); //delete old image

            }else{
                $newCategoryImage = $old_category_image->category_image;
            }

            if(isset($_FILES['round']))
            {
                $file = $_FILES['round'];
                $image = new Image();

                $fileExt = explode('.', $file['name']);
                $fileExt = end($fileExt);

                $roundImage = 'round_'.uniqid().'.'.$fileExt;
                $newRoundImage = 'admins/images/round_category/'.$roundImage;
                $image->resize_image($file, [ 'name' => $roundImage, 'width' => 108, 'height' => 108, 'size_allowed' => 1000000,'file_destination' => 'admins/images/round_category/' ]);
                
                if(!$image->passed())
                {
                    $error['round'] = $image->error();
                }
                Image::delete($old_category_image->round_cat_image); //delete old image

            }else{
                $newRoundImage = $old_category_image->round_cat_image;
            }
        }

        if(!empty($error))
        {
            return response()->json(['error' => $error]);
        }else{
            $newCategory = DB::table('categories')->where('category_id', $request->category_id)->update(array(
                                'category_name' => $request->category,
                                'category_image' => $newCategoryImage,
                                'round_cat_image' => $newRoundImage,
                            ));
             if($newCategory)
             {
                 $data = true;
             }
        }
        return response()->json(['data' => $data]);
    }





    // CATEGORY FEATURE
    public function admin_category_feature_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $category = DB::table('categories')->where('category_id', $request->category_id)->first();
            $is_feature = $category->is_feature ? 0 : 1;

            $update = DB::table('categories')->where('category_id', $request->category_id)->update(array(
                        'is_feature' => $is_feature
                    ));
            if($update)
            {
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }






    public function admin_delete_feature_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $error = null;

            $category = DB::table('categories')->where('category_id', $request->category_id);
            if(!Image::delete($category->first()->round_cat_image))
            {
                $error['delete_error'] = 'There was an error';
            }
            if(!Image::delete($category->first()->category_image))
            {
                $error['delete_error'] = 'There was an error';
            }
            if(empty($error))
            {
                $category->delete();
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }











    // end
}



