<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Admin\Auth;
use App\Models\Admin\Admin;
use App\Models\Admin\Image;
use App\Models\Validate;
use DB;
use Session;


class AdminCustomerController extends Controller
{
    public function index()
    {
        $admin = Admin::where('token',  Auth::admin()['token'])->where('id',  Auth::admin()['id'])->first();
       
        if(!$admin)
        {
            $admin = null;
        }

        // get all users
        $customers = DB::table('users')->paginate(15);

        return view('admin.customer', compact('admin', 'customers'));
    }




    public function admin_add_customer_ajax(Request $request)
    {
        if($request->ajax())
        {
            $error = null;
            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'first_name' => 'required|min:3|max:50',
                'last_name' => 'required|min:3|max:50',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6|max:12',
                'confirm_password' => 'required|min:6|max:12|match:password',
            ]);

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }

            if($validation->passed())
            {
                $demo_image = 'admins/images/users/demo.png';
                $create = DB::table('users')->insert(array(
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'password' => password_hash($request->password, PASSWORD_DEFAULT),
                    'user_image' => $demo_image,
                ));
                if($create)
                {
                    $data = true;
                }
            }

        }
        return response()->json(['data' => $data]);
    }




    public function get_all_customer_ajax(Request $request)
    {
        if($request->ajax())
        {
            //get all customers
            $customers = DB::table('users')->paginate(15);
            return view('admin.common.ajax-customers-table', compact('customers'));
        }
    }




    public function get_edit_customer_detail_ajax(Request $request)
    {
        if($request->ajax())
        {
            // get customer edit detail
            $data = DB::table('users')->where('id', $request->id)->first();
        }
        return response()->json(['data' => $data]);
    }
    



    public function admin_edit_customer_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            // get customer edit detail
            $customer = DB::table('users')->where('id', $request->id)->first();
            if($customer)
            {
                $validate = new Validate();
                $validation = $validate->validate([
                    'first_name' => 'required|min:3|max:50',
                    'last_name' => 'required|min:3|max:50',
                    'email' => 'required|email',
                    'password' => 'min:6|max:12',
                    'confirm_password' => 'min:6|max:12',
                ]);
    
                if(!$validation->passed())
                {
                    return response()->json(['error' => $validation->error()]);
                }

                if($customer->email != $request->email)
                {
                    $customers = DB::table('users')->where('email', $request->email)->first();
                    if($customers)
                    {
                        return response()->json(['error' => ['email' => '*email already exist']]); 
                    }
                } 

                $update = DB::table('users')->where('id', $request->id)->update(array(
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                ));
                $data = true;

                if(!empty($request->password))
                {
                    $validatePassword = new Validate();
                    $passwordCheck = $validatePassword->validate([
                        'password' => 'min:6|max:12',
                        'confirm_password' => 'min:6|max:12|match:password',
                    ]);

                    if(!$passwordCheck->passed())
                    {
                        return response()->json(['error' => $passwordCheck->error()]);
                    }else{
                        $update = DB::table('users')->where('id', $request->id)->update(array(
                            'password' => password_hash($request->password, PASSWORD_DEFAULT)              
                        ));
                        $data = true;
                    }
                }
            }
        }
        return response()->json(['data' => $data]);
    }





    public function admin_delete_customer_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            // get customer edit detail
            $customer = DB::table('users')->where('id', $request->id)->first();
            if($customer)
            {
                $demo_image = 'admins/images/users/demo.png';
                if($customer->user_image != $demo_image)
                {
                    Image::delete($customer->user_image);
                    $update = DB::table('users')->where('id', $request->id)->update(array(
                        'user_image' => $demo_image
                    ));
                }
               
                return view('admin.common.ajax-customer-image', compact('customer'));
            }
        }
    }




    public function admin_delete_customer_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            // get customer edit detail
            $customer = DB::table('users')->where('id', $request->id)->first();
            if($customer)
            {
                $demo_image = 'admins/images/users/demo.png';
                if($customer->user_image != $demo_image)
                {
                    Image::delete($customer->user_image);
                }
                DB::table('users')->where('id', $request->id)->delete();
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }






    // DEACTIVATE CUSTOMER AJAX
    public function admin_deactivate_customer_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $is_deactivate = 1;
            $id = explode('_', $request->id)[1];
            $customer = DB::table('users')->where('id', $id)->first();
            if($customer)
            {
               if($customer->is_deactivate)
               {
                   $is_deactivate = 0;
               }
               $deactivate = DB::table('users')->where('id', $id)->update(array(
                   'is_deactivate' => $is_deactivate
               ));
               $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }





// end
}