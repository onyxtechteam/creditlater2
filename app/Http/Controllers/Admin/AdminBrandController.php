<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Admin\Auth;
use App\Models\Admin\Admin;
use DB;
use Session;


class AdminBrandController extends Controller
{
    public function index()
    {
        if(!Session::has('admin'))
        {
            return redirect('register');
        }

        // get brands
        $brands = DB::table('brands')->paginate(15);
        if(count($brands) == "")
        {
            $brands = null;
        }

        $admin = Admin::where('token',  Auth::admin()['token'])->where('id',  Auth::admin()['id'])->first();
       
        if(!$admin)
        {
            $admin = null;
        }


        return view('admin.brands', compact('brands', 'admin'));
    }





    public function brand_approved($id)
    {
        if($id)
        {
            $category = DB::table('brands')->where('brand_id', $id)->first();
            if($category)
            {
                $is_approved = $category->is_approved ? 0 : 1;
                DB::table('brands')->where('brand_id', $id)->update(array(
                      'is_approved' => $is_approved
                ));
            }
        }
        return back();
    }


    



    public function add_brand(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $error = null;
            if(empty($request->brand))
            {
                $error['brand'] = '*Brand field is required';
            }else if(strlen($request->brand) > 50){
                $error['brand'] = '*Brand must be maximum of 50 characters';
            }else{
                $old_brand = DB::table('brands')->where('brand_name', $request->brand)->first();
                if($old_brand)
                {
                    $error['brand'] = '*The brand '.$request->brand.' already exist';
                }
            }
            
            if(!empty($error))
            {
                return response()->json(['error' => $error]);
            }else{
                DB::table('brands')->insert(array(
                    'brand_name' => $request->brand,
                ));
                $data = true;
            }
            
        }
        return response()->json(['data' => $data]);
    }






    // GET BRAND AJAX
    public function get_brand_ajax(Request $request)
    {
        if($request->ajax())
        {
            $brands = DB::table('brands')->get();
            return view('admin.common.ajax-get-brand', compact('brands'));
        }
    }




    // DELETE BRAND AJAX
    public function delete_brand_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $brands = DB::table('brands')->where('brand_id', $request->brand_id)->delete();
            if($brands)
            {
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }






    public function admin_edit_brand_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $error = null;
            if(empty($request->brand))
            {
                $error['brand'] = '*Brand field is required';
            }else if(strlen($request->brand) > 50){
                $error['brand'] = '*Brand must be maximum of 50 characters';
            }

            $old_brand = DB::table('brands')->where('brand_id', $request->brand_id)->where('brand_name', $request->brand)->first();
            if(!$old_brand)
            {
                $old_brands = DB::table('brands')->where('brand_name', $request->brand)->get();
                if(count($old_brands) > 0)
                {
                   $error['brand'] = '*The brand '.$request->brand.' already exist';   
                }
            }
            
            if(!empty($error))
            {
                return response()->json(['error' => $error]);
            }else{
                 DB::table('brands')->where('brand_id', $request->brand_id)->update(array(
                    'brand_name' => $request->brand
                 ));
                $data = true;
            }
            
        }
        return response()->json(['data' => $data]);
    }







    public function admin_feature_brand_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $is_feature =   DB::table('brands')->where('brand_id', $request->brand_id)->first();
            $value = $is_feature->is_feature == 1 ? 0 : 1;
            $feature = DB::table('brands')->where('brand_id', $request->brand_id)->update(array(
                        'is_feature' => $value
                    ));
            if($feature)
            {
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }

    


    // end
}
