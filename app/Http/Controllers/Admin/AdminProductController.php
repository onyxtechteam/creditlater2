<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Admin\Auth;
use App\Models\Admin\Admin;
use App\Models\Validate;
use App\Models\Admin\Image;
use DB;
use Session;

class AdminProductController extends Controller
{
    public function index()
    {

        if(!Session::has('admin'))
        {
            return redirect('admin/login');
        }


        // get products
        $products = DB::table('products')->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
                            ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->paginate(15);
      

        $categories = DB::table('categories')->get(); // get all categories

        $brands = DB::table('brands')->get(); //get all brands

        $admin = Admin::where('token',  Auth::admin()['token'])->where('id',  Auth::admin()['id'])->first();
       
        if(!$admin)
        {
            $admin = null;
        }


        return view('admin.products', compact('products', 'admin', 'brands', 'categories'));
    }




    public function product_approve($id)
    {
        if($id)
        {
            $product = DB::table('products')->where('id', $id)->first();
            if($product)
            {
                $is_approve = $product->is_approve ? 0 : 1;
                DB::table('products')->where('id', $id)->update(array(
                      'is_approve' => $is_approve
                ));
            }
        }
        return back();
    }





    public function admin_add_product_ajax(Request $request)
    {
        if($request->ajax())
        {   
            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'name' => 'required|min:3|max:50',
                'price' => 'required',
                'price_slash' => 'required',
                'quantity' => 'required',
                'initial' => 'required',
                'detail' => 'required|min:3',
                'description' => 'required|min:3',
                'shipping' => 'required'
            ]);

            if($request->initial > $request->price)
            {
                return response()->json(['error' => ['initial' => '*Initial price must not be greater than price!']]);
            }

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }else if($request->price > $request->price_slash)
            {
                return response()->json(['error' => ['price' => '*Price must not be greater than price slash!']]);
            }else{

                if(!Image::post('image'))
                {
                    return response()->json(['error' => ['image' => '*image field is required']]);
                }else{
                    $file = Image::get('image');
                    $fileName = Image::name($file, 'product');
                    $file_path = 'admins/images/product/'.$fileName;
    
                    $image = new Image();
                    $image->resize_image($file, [ 'name' => $fileName, 'width' => 768, 'height' => 988, 'size_allowed' => 1000000,'file_destination' => 'admins/images/product/' ]);
                        
                    if(!$image->passed())
                    {
                        return response()->json(['error' => ['image' => $image->error()]]);
                    }else{
                        $data = true;
                    }
                }
            }

            if($data)
            {
                $sizes = $this->get_products_sizes($request);
                $is_feature = $request->feature ? $request->feature : 0;
                $is_special = $request->special ? $request->special : 0;
                $warranty = $request->warranty ? $request->warranty : null;
                $product_slug = DB::table('categories')->where('category_id', $request->category)->first();

                $create = DB::table('products')->insert(array(
                    'admin_id' => Auth::admin()['id'],
                    'products_name' => $request->name,
                    'brand_id' => $request->brand,
                    'category_id' => $request->category,
                    'products_slug' => $product_slug->category_name,
                    'products_price' => $request->price,
                    'products_quantity' => $request->quantity,
                    'products_price_slash' => $request->price_slash,
                    'initial_payment' => $request->initial,
                    'products_video_link' => $request->video_link,
                    'products_type' => $sizes,
                    'shipping_method' => $request->shipping,
                    'warranty' => $warranty,
                    'is_special' => $is_special,
                    'is_product_feature' => $is_feature,
                    'products_detail' => $request->detail,
                    'products_description' => $request->description,
                    'products_image' => $file_path,
                ));

                if($create)
                {
                    $data = true;
                }
            } 
            return response()->json(['data' => $data]);   
        }
    }






    public function get_products_sizes($request)
    {
        if($request)
        {
            $sizes = [];
            $new_size = null;
            $sizes[] = $request->small ? $request->small : '';
            $sizes[] = $request->medium ? $request->medium : '';
            $sizes[] = $request->large ? $request->large : '';
            $sizes[] = $request->xl ? $request->xl : '';


            foreach($sizes as $size)
            {
                if($size != '')
                {
                    $new_size[] = $size;
                }
            }

            if($new_size != '' && count($new_size) > 0)
            {
                $new_size = implode(',', $new_size);
            }
            return $new_size;
        }
         return false;
    }





    //  GET ALL PRODUCTS
    public function get_all_product_ajax(Request $request)
    {
        if($request->ajax())
        {
           // get products
            $products = DB::table('products')->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
            ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->paginate(15);

            if(count($products) == "")
            {
                $products = null;
            }

        }
        return view('admin.common.ajax-get-products', compact('products'));
    }






    // FEATURE PRODUCTS
    public function get_feature_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $product_id = (int)explode('_', $request->product_id)[1];

            $product = DB::table('products')->where('id', $product_id)->update(array(
                'is_product_feature' => $request->is_feature
            ));

            if($product)
            {
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }






    // DELETE PRODUCT
    public function delete_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $product = DB::table('products')->where('id', $request->product_id)->first();
            if($product)
            {
                $products_image = explode(',', $product->products_image);
                if(count($products_image) > 0)
                {
                    foreach($products_image as $image)
                    {
                        Image::delete($image);
                    }
                }
                $delete_products = DB::table('products')->where('id', $product->id)->delete();
                if($delete_products)
                {
                    $data = true;
                }
            }
        }
        return response()->json(['data' => $data]);
    }







    public function edit_product_ajax(Request $request)
    {
        if($request->ajax())
        {
           // get products
           $product = DB::table('products')->where('id', $request->product_id)->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
           ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->first();

           $categories = DB::table('categories')->get(); // get all categories

           $brands = DB::table('brands')->get(); //get all brands

            if($product)
            {
                return view('admin.common.ajax-edit-product-modal', compact('product', 'categories', 'brands'));
            }
        }
    }






    public function update_product_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            if(Image::post('image'))
            {
                $store_images = [];
                $product = DB::table('products')->where('id', $request->product_id)->first();
                
                $file = Image::get('image');
                $fileName = Image::name($file, 'product');
                $file_path = 'admins/images/product/'.$fileName;

                $image = new Image();
                $image->resize_image($file, [ 'name' => $fileName, 'width' => 768, 'height' => 988, 'size_allowed' => 1000000,'file_destination' => 'admins/images/product/' ]);
                    
                if(!$image->passed())
                {
                    return response()->json(['error' => ['image' => $image->error()]]);
                }else{

                    $images = $file_path;
                    if($product->products_image)
                    {
                        $store_images = explode(',', $product->products_image);
                        $store_images[] = $file_path;
                        $images = implode(',', $store_images);
                    }
                   
                    $updateProduct = DB::table('products')->where('id', $request->product_id)->update(array(
                        'products_image' => $images
                    ));
                    if($updateProduct)
                    {
                        $data = true;
                    }
                }
                
            }
        }
        return response()->json(['data' => $data]);
    }







    // GET IMAGE TOTAL
    public function get_total_product_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $product = DB::table('products')->where('id', $request->product_id)->first();
        }
        return view('admin.common.ajax-product-edit-image', compact('product'));
    }




    public function delete_product_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $deleteImage = null;
            $key = $request->key;
            $product = DB::table('products')->where('id', $request->product_id)->first();
            if($product)
            {
                $images_array = explode(',', $product->products_image);
                if(array_key_exists($key, $images_array))
                {
                    $images = null;
                    $image_name = str_replace(' ','', $images_array[$key]);
                
                    if(!Image::delete($image_name))
                    {
                        return response()->json(['error' => ['image' => '*image could not be deleted!']]);
                    }

                    unset($images_array[$key]);
                    
                    if(count($images_array))
                    {
                        $images = implode(',', $images_array);
                    }
                    $data = $images;

                    $update_image = DB::table('products')->where('id', $request->product_id)->update(array(
                        'products_image' => $images
                    ));
                    if($update_image)
                    {
                        $data = true;
                    }
                    
                }
            }
        }
        return response()->json(['data' => $data]);
    }






    // EDIT PRODUCT
    public function admin_edit_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'name' => 'required|min:3|max:50',
                'price' => 'required',
                'price_slash' => 'required',
                'quantity' => 'required',
                'initial' => 'required',
                'detail' => 'required|min:3',
                'description' => 'required|min:3',
                'shipping' => 'required'
            ]);

            if($request->initial > $request->price)
            {
                return response()->json(['error' => ['initial' => '*Initial price must not be greater than price!']]);
            }

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }else if($request->price > $request->price_slash)
            {
                return response()->json(['error' => ['price' => '*Price must not be greater than price slash!']]);
            }else{
               $sizes = $this->get_products_sizes($request);
               
               $is_feature = $request->feature ? $request->feature : 0;
               $is_special = $request->special ? $request->special : 0;
               $warranty = $request->warranty ? $request->warranty : null;
               $product_slug = DB::table('categories')->where('category_id', $request->category)->first();

               $update = DB::table('products')->where('id', $request->product_id)->update(array(
                                'products_name' => $request->name,
                                'brand_id' => $request->brand,
                                'category_id' => $request->category,
                                'products_slug' => $product_slug->category_name,
                                'products_price' => $request->price,
                                'products_quantity' => $request->quantity,
                                'products_price_slash' => $request->price_slash,
                                'initial_payment' => $request->initial,
                                'products_video_link' => $request->video_link,
                                'products_type' => $sizes,
                                'shipping_method' => $request->shipping,
                                'warranty' => $warranty,
                                'is_special' => $is_special,
                                'is_product_feature' => $is_feature,
                                'products_detail' => $request->detail,
                                'products_description' => $request->description,
                            ));

                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }






    public function admin_approved_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $product_id = explode('_', $request->product_id)[1];
            $product = DB::table('products')->where('id', $product_id)->first();
            
            $approved = $product->is_approve ? 0 : 1;
            $update = DB::table('products')->where('id', $product_id)->update(array(
                        'is_approve' => $approved
                    ));
            if($update)
            {
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }





    // end
}
