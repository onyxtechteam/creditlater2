<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Web\Category;
use App\Models\Web\Auth;
use App\Models\Web\User;
use App\Models\Image;
use App\Models\Validate;

use DB;
use Session;



class ProductReturnController extends Controller
{
    public function index($product_id, $product_token)
    {
        //  side categories
        $sideCategories = Category::where('is_feature', 1)->get();

        return view('web.return', compact('sideCategories'));
    }





    public function store(Request $request)
    {
        if(Auth::user())
        {
            $validate = $request->validate([
                'email' => 'required|email',
                'warranty_number' => 'required',
                'warranty_slip' => 'required'
            ]);


            $user = User::where('email', $request->email)->first();
            if($user)
            {
                $warranty_slip = $request->file('warranty_slip');
                $newImageName = 'warranty_file_'.time().'.'.$warranty_slip->getClientOriginalExtension();
                $Destination = public_path('/admin/images/warranty_file');
                $W_ImagePath = "/admin/images/warranty_file/".$newImageName;
               
                if($warranty_slip->move($Destination, $newImageName))
                {
                    $return = DB::table('return_products')->insert(array(
                        'buyer_id' => $user->id,
                        'email' => $request->email,
                        'warranty_number' => $request->warranty_number,
                        'warranty_slip' =>  $W_ImagePath
                    ));

                    if($return){
                        return redirect("account")->with('success', "Your request would be attended to shortly!");
                    }
                }

            }else{
                return back()->with('error', "Email does not exist!");
            }
        }else{
            return back()->with('error', "Login or Signup to return a product!");
        }
        return back();
    }








    public function return_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'email' => 'required|email',
                'quantity' => 'required',
                'message' => 'required|min:3',
            ]);


            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }

            if(!Image::post('warranty'))
            {
                return response()->json(['error' => ['warranty' => '*Warranty field is required!']]);
            }

            if(!is_numeric($request->quantity))
            {
                return response()->json(['error' => ['quantity' => '*Quantity must be numeric!']]);
            }


            $product = DB::table('paids')->where('paids_token', $request->token)->where('product_id', $request->product_id)->first();
            if($request->quantity > $product->quantity)
            {
                return response()->json(['error' => ['quantity' => '*Quantity must be maximum of '.$product->quantity]]);
            }

           if(Image::post('warranty'))
            {
                $file = Image::get('warranty');
                $image = new Image();

                $fileName = Image::name($file, 'warranty');                
                $image->upload_image($file, [ 'size_allowed' => 1000000, 'file_destination' =>  'admins/images/warranty_file/']);
                if(!$image->passed())
                {
                    return response()->json(['error' => ['warranty' => $image->error()]]);
                }
            }

            $product_return = DB::table('return_products')->insert(array(
                  'buyer_id' => Auth::user('id'),
                  'w_buyer_token' => $request->token,
                  'w_product_id' => $request->product_id,
                  'w_email' => $request->email,
                  'w_quantity' => $request->quantity,
                  'w_product_price' => $product->price,
                  'w_message' => $request->message,
                  'w_total_price' => $product->price * $request->quantity,
                  'warranty_slip' => 'admins/images/warranty_file/'.$image->file_name()
            ));

            if($product_return)
            {
                $product = DB::table('paids')->where('paids_token', $request->token)
                            ->where('product_id', $request->product_id)->update(array(
                                 'product_return_qty' => $request->quantity
                            ));
                if($product)
                {
                    Session::flash('success', 'Product has been returned successfully!');
                    $data = true;
                }
            }
        }
        return response()->json(['data' => $data]);
    }






    public function return_history_index()
    {
        if(!Auth::is_loggedin())
        {
            Session::put('old_url', current_url());
            return redirect('login');
        }
        
        //  side categories
        $sideCategories = Category::where('is_feature', 1)->get();

        $user = DB::table('users')->where('email', Auth::user('email'))->where('id', Auth::user('id'))->first();

        $return_histories = DB::table('return_products')->where('buyer_id', Auth::user('id'))->where('w_email', Auth::user('email'))->get();

       return view('web.return-history', compact('sideCategories', 'return_histories', 'user'));
    }

    // end
}
