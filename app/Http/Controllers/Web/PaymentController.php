<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use App\Models\Web\User;
use App\Models\Web\Auth;
use App\Models\Web\Paid;
use App\Models\Web\Category;
use App\Models\Web\PaidBuyer;
use App\Models\Web\Paystack;
use App\Models\Web\Product;
use Illuminate\Support\Facades\Hash;
use App\Models\Validate;


use DB;
use Session;
use Carbon\Carbon;
use Redirect;
use Validator;

class PaymentController extends Controller
{

   
    public function checkout_one_time_payment_paystack(Request $request)
    {
        if(!Auth::user())
        {
            return redirect('web/login')->with('error', 'Login or signup to buy a product');
        }
        $buyer_detail = Session::get('buyer_details');
        $amount = $buyer_detail['initial'] ? $buyer_detail['initial'] : $buyer_detail['total'];
      
        $callback_url = url('/order-success');     
        Paystack::initialize($buyer_detail['email'], $amount, $callback_url);
    }




    // checkout view
    public function checkout_view()
    {
        if(Session::has('reference'))
        {
            return redirect('order-success');
        }
        if(!Session::has('cart'))
        {
            return redirect('/');
        }

        if(!Auth::user())
        {
            return redirect('/login')->with('alert', 'Signup or login to access that page!');
        }

        $installmentPeriods = DB::table('installment_period')->where('is_feature', 1)->get();
       
        //get all category
        $sideCategories = Category::where('is_feature', 1)->get();  


        return view('web.checkout', compact('sideCategories', 'installmentPeriods'));
    }








    public function get_installment_period_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $product = DB::table('products')->where('id', $request->product_id)->first();
            $period = DB::table('installment_period')->where('id', $request->period_id)->first();
            if($product)
            {
                $totalPrice = $product->products_price * $request->quantity;
                $percentage =  ($period->percentage / 100) * $totalPrice;
                $total = $percentage + $totalPrice;
                $dataTotal = $period->period.': '.money($total);

                $minimum_installment =  'Minimum Installment: '.money((35 / 100) * $total);

               $data = ['total' => $dataTotal, 'installment' => $minimum_installment];
            }
        }
        return response()->json(['data' => $data]);
    }






    public function get_installment_payment_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $period = DB::table('installment_period')->where('id', $request->period_id)->first();
            $totalPrice = Session::has('cart') ? Session::get('cart')->_totalPrice : false;

            if($period && $totalPrice)
            {
                $percentage =  ($period->percentage / 100) * $totalPrice;
                $total = $totalPrice + $percentage;
                $total_format = money($total);

                $initial =  ceil((35 / 100) * $total);
                $initial_installment =  money((35 / 100) * $total);

                $data = ['total' => $total , 'total_format' => $total_format, 'initial' => $initial, 'initial_installment' => $initial_installment];
            }
        }
        return response()->json(['data' => $data]);
    }









    public function pay_now_ajax(Request $request)
    {
        if($request->ajax())
        {
            // if installment payment check if user is verified
            $verified = DB::table('verifications')->where('user_id', Auth::user('id'))->first();
            if($request->initial && !$verified)
            {
                return response()->json(['verification' => url('/verification') ]);
            }

            if(!$verified->is_approved && $request->initial)
            {
                return response()->json(['is_approved' => 'You have not been approved for installment yet!']);
            }

            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'first_name' => 'required|min:3|max:50',
                'last_name' => 'required|min:3|max:50',
                'phone' => 'required|min:11|max:15',
                'email' => 'required|email',
                'address' => 'required|min:3|max:200',
                'city' => 'required|min:3|max:50',
                'state' => 'required|min:3|max:50',
                'country' => 'required|min:3|max:50',
                'postal_code' => 'min:3|max:50',
            ]);

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }
            
            if($validation->passed())
            {
                Session::put('buyer_details', $request->all());
                $data = true;
            }
            
        }
        return response()->json(['data' => $data]);
    }











    public function store_paid_products($reference, $token)
    {
        if($reference)
        {
            $user = Auth::user();
            $stored_user = User::where('id', $user['id'])->where('email', $user['email'])->first();
            if($user)
            {
                $cart_items = Session::get('cart')->_items;
                foreach($cart_items as $items)
                {
                    $paid = new Paid();
                    $paid->create([
                        'buyer_user_id' => $user['id'],
                        'reference_number' => $reference,
                        'paids_token' => $token,
                        'product_id' => $items['product_id'],
                        'price' =>  $items['price'],
                        'quantity' =>  $items['quantity'],
                        'total' =>  $items['total'],
                        'size' => $items['size']
                    ]);
                }

                foreach($cart_items as $items)
                {
                    $product = Product::where('id', $items['product_id'])->first();
                    $product->products_quantity -= $items['quantity'];
                    $product->quantity_sold  += $items['quantity'];
                    $product->save();
                }
                Session::forget('cart');
                return true;
            }
        }
        return false;
    }








    public function store_buyers_details($reference)
    {
        if(Session::has("buyer_details"))
        {
            $token = uniqid().''.Auth::user()['id'];
            $buyer_details = Session::get('buyer_details');
            $completed = $buyer_details['initial'] ? 0 : 1;
            $postal_code = $buyer_details['postal_code'] ? $buyer_details['postal_code'] : null;

            $initial = $buyer_details['initial'] ? $buyer_details['initial'] : null;
            $balance = $buyer_details['initial'] ? $buyer_details['total'] - $buyer_details['initial'] : 0;

            $type = $buyer_details['payment_type'] ? $buyer_details['payment_type'] : 1;  
            $payment_type = DB::table('installment_period')->where('id', $type)->first()->period;

            $store = new PaidBuyer();
            $store->user_id = Auth::user()['id'];
            $store->reference = $reference;
            $store->buyer_token = $token;
            $store->first_name = $buyer_details['first_name'];
            $store->last_name = $buyer_details['last_name'];
            $store->phone = $buyer_details['phone'];
            $store->state = $buyer_details['state'];
            $store->city = $buyer_details['city'];
            $store->address = $buyer_details['address'];
            $store->country = $buyer_details['country'];
            $store->email = $buyer_details['email'];
            $store->postal_code = $postal_code;
            $store->amount = $buyer_details['total'];
            $store->initial_payment = $initial;
            $store->buyer_balance = $balance;
            $store->is_completed = $completed;
            $store->payment_type = $payment_type;
            $store->save();
            
            $this->store_paid_products($reference, $token);  //store paid products
            $this->store_transaction($reference, $token);

            Session::forget('buyer_details');
            return true;
        }
    }






    public function store_transaction($reference, $token)
    {
        if($reference)
        {
            $buyer_details = Session::get('buyer_details');
            $balance = $buyer_details['initial'] ? $buyer_details['total'] - $buyer_details['initial'] : 0;
            $paid = $buyer_details['initial'] ? $buyer_details['initial'] : null;

            $transaction = DB::table('transactions')->insert(array(
                    'transaction_buyer_id' => Auth::user()['id'],
                    'total' => $buyer_details['total'],
                    'paid' => $paid,
                    'balance' => $balance,
                    'transction_reference' => $reference,
                    'transaction_token' => $token
            ));
            
            if($transaction)
            {
                return true;
            }
        }
        return false;
    }






    public function get_loggedin_user_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = true;
            $user = Auth::user();
            $loggedIn = User::where('id', $user['id'])->where('email', $user['email'])->where('active', 1)->first();
             if(!$loggedIn)
             {
                Session::put('cart_url', url('/cart'));
                Session::flash('alert', "login or signup to purchase items!");
                $data = null;
             }
        }
        return response()->json(['data' => $data]);
    }








    public function order_success()
    {
        $sideCategories = Category::where('is_feature', 1)->get();  

        if(!Session::has('buyer_details') && !Session::has('installment_details'))
        {
            return redirect('/');
        }
        
        if(Session::has('buyer_details'))
        {
            $reference = Paystack::call_back();
            // $reference = 1234567890;
            $this->store_buyers_details($reference);  //store buyers details

            // $order = DB::table('paid_buyers')->where('reference', $reference)->where('user_id', '=', Auth::user()['id'])->get();
        }

        return view('web.order-success', compact('sideCategories'));
    }









    // COMPLETE INSTALLMENT PAYMENT
    public function complete_installment_payment_ajax(Request $request)
    {
        if($request->ajax())
        {
            $validate = new Validate();
            $validation = $validate->validate([
                'email' => 'required|email',
                'password' => 'required|min:6|max:12',
                'amount' => 'required',
            ]);
            
            $balance = DB::table('paid_buyers')->where('buyer_token', $request->buyer_token)->first();
            if($balance->buyer_balance < $request->amount)
            {
                return response()->json(['error' => ['amount' => 'Amount must not be greater than balance!']]);
            }

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }
            

            if($request->email != Auth::user('email'))
            {
                return response()->json(['error' => ['email' => 'Wrong email, try again!']]);
            }
            
            $user = DB::table('users')->where('email', Auth::user('email'))->first();
            if(!password_verify($request->password, $user->password))
            {
                return response()->json(['error' => ['password' => 'Wrong password, try again!']]);
            }

            
            if($validation->passed())
            {    
                Session::put('installment_details', $request->all());
                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }










    public function installment_paystack_init(Request $request)
    {
        if(Session::has('installment_details'))
        {
            $installemtn_details = Session::get('installment_details');
            $callback_url = url('/order-detail/'.$installemtn_details['buyer_token']);     
            Paystack::initialize($installemtn_details['email'], $installemtn_details['amount'], $callback_url);
        }

        //go to store order details in orderController to view remaining success codes
    }






  




    // end
}