<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Web\User;
use App\Models\Web\PaidBuyer;
use App\Models\Web\Category;
use App\Models\Web\Auth;

use DB;
use Session;

class OrderController extends Controller
{
    public function index()
    {
         //get all category
         $sideCategories = Category::where('is_feature', 1)->get(); 
         
        //get buyers orders
        $buyer_order = null;
        if(Auth::user())
        {
            $user_id = Auth::user()['id'];
            $buyer_orders = DB::table('paid_buyers')->where('user_id', $user_id)->get();
        }else{
            return redirect('/')->with('alert', 'Signup or login to access that page!');
        }

         return view('web.order_history', compact('sideCategories', 'buyer_orders'));
    }




    public function show()
    {
        //get all category
        $sideCategories = Category::where('is_feature', 1)->get(); 
         
        //get buyers orders
        $buyer_order = null;
        if(Auth::user())
        {
            $user_id = Auth::user()['id'];
            $buyer_order = DB::table('paids')->where('buyer_user_id', $user_id)
                          ->leftJoin('products', 'paids.product_id', '=', 'products.id')->orderBy('product_id', 'desc')->get();
            if(count($buyer_order) == "")
            {
                $buyer_order = null;
            }
        }
       

         return view('web.order_history', compact('sideCategories', 'buyer_order'));
    }





    public function completement_payment_show()
    {
        //get all category
        $sideCategories = Category::where('is_feature', 1)->get(); 


        return view('web.complete_payments', compact('sideCategories'));
    }





    public function get_order_detail_show($buyer_token)
    {
        if(!Auth::user())
        {
            return redirect('/');
        }

        if(Session::has('installment_details'))
        {
           $reference = Paystack::call_back();
        //    $reference = 1234567890;
           $this->store_installment_details($reference);  //store installment details
        }


        //get all category
        $sideCategories = Category::where('is_feature', 1)->get(); 

        $user = DB::table('users')->where('email', Auth::user('email'))->where('id', Auth::user('id'))->first();
        

        $user_id =  Auth::user()['id'];

        $order_detail = DB::table('paid_buyers')->where('user_id', $user_id)->where('buyer_token', $buyer_token)->first();
        $token = $order_detail->buyer_token;

        $paid_products = DB::table('paids')->where('buyer_user_id', $user_id)->where('paids_token', $token)
                        ->leftJoin('products', 'paids.product_id', '=', 'products.id')->get();

        $transactions = DB::table('transactions')->where('transaction_buyer_id', $user_id)->where('transaction_token', $token)->get();
         
        
        return view('web.order-detail', compact('sideCategories', 'order_detail', 'paid_products', 'transactions', 'user'));
    }






    public function store_installment_details($reference)
    {
        if($reference)
        {
            $details = Session::get('installment_details');

            $storeBuyer = DB::table('paid_buyers')->where('buyer_token', $details['buyer_token'])->first();
            $balance = $storeBuyer->buyer_balance - $details['amount'];
            $completed = $balance <= 0 ? 1 : 0;

            if($storeBuyer)
            {
                $transaction = DB::table('transactions')->insert(array(
                    'transaction_buyer_id' => Auth::user()['id'],
                    'total' => $storeBuyer->amount,
                    'paid' => $details['amount'],
                    'balance' => $balance,
                    'transction_reference' => $reference,
                    'transaction_token' => $details['buyer_token']
                ));

                if($transaction)
                {
                    $storeBuyer = DB::table('paid_buyers')->where('buyer_token', $details['buyer_token'])->update(array(
                        'buyer_balance' => $balance,
                        'is_completed' => $completed
                    ));
                    Session::forget('installment_details');
                    Session::flash('success', "Payment has been made successfully!");
                }
            }
              
        }
    }



    // end
}
