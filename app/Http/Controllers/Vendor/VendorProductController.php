<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Image;
use App\Models\Validate;
use DB;
use Session;

class VendorProductController extends Controller
{
    public function index()
    {
        if(!Session::has('vendor'))
        {
            return redirect('vendor/login');
        }
        // get products
        $products = DB::table('products')->where('vendor_id', Session::get('vendor')['id'])->where('is_approve', 1)->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
                            ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->paginate(15);
     
        $brands = DB::table('brands')->where('is_feature', 1)->get(); //get all brands;
        $categories = DB::table('categories')->where('is_feature', 1)->get(); //get all categories;

        return view('vendor.products', compact('products', 'categories', 'brands'));
    }

   



    public function product_return_show()
    {
        $return_products = DB::table('return_products')->leftJoin('users', 'return_products.buyer_id', '=', 'users.id')->get();
        if(count($return_products) == '')
        {
        $return_products = null;
        }
    
        return view('vendor.product_return', compact('return_products'));
    }




    // MAXIMIZE WARRANTY SLIP
    public function maximize_warranty_slip_ajax(Request $request)
    {
        if($request->ajax())
        {
            $warranty = DB::table('return_products')->where('id', $request->warranty_id)->first();
            // return to the dropdown page with the image
            // i stopped here
            $data = true;
        }
        return response()->json(['data' => $data]);
    }





    public function vendor_add_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'name' => 'required|min:3|max:50',
                'price' => 'required',
                'price_slash' => 'required',
                'quantity' => 'required',
                'initial' => 'required',
                'detail' => 'required|min:3',
                'description' => 'required|min:3',
                'shipping' => 'required'
            ]);

            
            if($request->initial > $request->price)
            {
                return response()->json(['error' => ['initial' => '*Initial price must not be greater than price!']]);
            }

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }else if($request->price > $request->price_slash)
            {
                return response()->json(['error' => ['price' => '*Price must not be greater than price slash!']]);
            }else{

                if(!Image::post('image'))
                {
                    return response()->json(['error' => ['image' => '*image field is required']]);
                }else{
                    $file = Image::get('image');
                    $fileName = Image::name($file, 'product');
                    $file_path = 'vendors/images/products_image/'.$fileName;
    
                    $image = new Image();
                    $image->resize_image($file, [ 'name' => $fileName, 'width' => 768, 'height' => 988, 'size_allowed' => 1000000,'file_destination' => 'vendors/images/products_image/' ]);
                        
                    if(!$image->passed())
                    {
                        return response()->json(['error' => ['image' => $image->error()]]);
                    }else{
                        $data = true;
                    }
                }

                if($data)
                {
                    $sizes = $this->get_products_sizes($request);
                    $is_feature = $request->feature ? $request->feature : 0;
                    $is_special = $request->special ? $request->special : 0;
                    $warranty = $request->warranty ? $request->warranty : null;
                    $product_slug = DB::table('categories')->where('category_id', $request->category)->first();
    
                    $create = DB::table('products')->insert(array(
                        'vendor_id' => Session::get('vendor')['id'],
                        'products_name' => $request->name,
                        'brand_id' => $request->brand,
                        'category_id' => $request->category,
                        'products_slug' => $product_slug->category_name,
                        'products_price' => $request->price,
                        'products_quantity' => $request->quantity,
                        'products_price_slash' => $request->price_slash,
                        'initial_payment' => $request->initial,
                        'products_video_link' => $request->video_link,
                        'products_type' => $sizes,
                        'shipping_method' => $request->shipping,
                        'warranty' => $warranty,
                        'is_special' => $is_special,
                        'is_product_feature' => $is_feature,
                        'products_detail' => $request->detail,
                        'products_description' => $request->description,
                        'products_image' => $file_path,
                    ));
                    
                    if($create)
                    {
                        $data = true;
                    }
                }
            }
        }
        return response()->json(['data' => $data]);
    }






    public function get_products_sizes($request)
    {
        if($request)
        {
            $sizes = [];
            $new_size = null;
            $sizes[] = $request->small ? $request->small : '';
            $sizes[] = $request->medium ? $request->medium : '';
            $sizes[] = $request->large ? $request->large : '';
            $sizes[] = $request->xl ? $request->xl : '';


            foreach($sizes as $size)
            {
                if($size != '')
                {
                    $new_size[] = $size;
                }
            }

            if(!empty($new_size) && count($new_size) > 0)
            {
                $new_size = implode(',', $new_size);
            }
            return $new_size;
        }
         return false;
    }






    public function vendor_get_product_ajax(Request $request)
    {
        if($request->ajax())
        {
           // get products
            $products = DB::table('products')->where('vendor_id', Session::get('vendor')['id'])->where('is_approve', 1)->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
            ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->paginate(15);

            if(count($products) == "")
            {
                $products = null;
            }

        }
        return view('vendor.common.ajax-product-table', compact('products'));
    }






    public function vendor_get_pending_product()
    {
        // get products
        $products = DB::table('products')->where('vendor_id', Session::get('vendor')['id'])->where('is_approve', 0)->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
        ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->paginate(15);

        if(count($products) == '')
        {
        $products = null;
        }

        return view('vendor.pending_product', compact('products'));
    }






    public function vendor_feature_product(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $product_id = explode('_', $request->product_id)[1];
            $product = DB::table('products')->where('id', $product_id)->where('is_approve', 1)->first();
            if($product)
            {
                $feature = $product->is_product_feature ? 0 : 1;
                $update = DB::table('products')->where('id', $product_id)->update(array(
                            'is_product_feature' => $feature
                        ));
                if($update)
                {
                    $data = true;
                }
            } 
        }
        return response()->json(['data' => $data]);
    }





    public function vendor_delete_product(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $product = DB::table('products')->where('id', $request->product_id)->first();
            if($product)
            {
                $products_image = explode(',', $product->products_image);
                if(count($products_image) > 0)
                {
                    foreach($products_image as $image)
                    {
                        Image::delete($image);
                    }
                }
                $delete_products = DB::table('products')->where('id', $product->id)->delete();
                if($delete_products)
                {
                    $data = true;
                }
            }
        }
        return response()->json(['data' => $data]);
    }




    // GET PRODUCT FOR EDIT MODAL
    public function vendor_get_edit_product_ajax(Request $request)
    {
        if($request->ajax())
        {
           // get products
           $product = DB::table('products')->where('id', $request->product_id)->where('vendor_id', Session::get('vendor')['id'])->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
           ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->first();

           $categories = DB::table('categories')->where('is_feature', 1)->get(); // get all categories

           $brands = DB::table('brands')->where('is_feature', 1)->get(); //get all brands

            if($product)
            {
                return view('vendor.common.ajax-edit-product-modal', compact('product', 'categories', 'brands'));
            }
        }
    }






    public function vendor_update_product_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            if(Image::post('image'))
            {
                $store_images = [];
                $product = DB::table('products')->where('id', $request->product_id)->first();
                
                $file = Image::get('image');
                $fileName = Image::name($file, 'product');
                $file_path = 'vendors/images/products_image/'.$fileName;

                $image = new Image();
                $image->resize_image($file, [ 'name' => $fileName, 'width' => 768, 'height' => 988, 'size_allowed' => 1000000,'file_destination' => 'vendors/images/products_image/' ]);
                    
                if(!$image->passed())
                {
                    return response()->json(['error' => ['image' => $image->error()]]);
                }else{

                    $images = $file_path;
                    if($product->products_image)
                    {
                        $store_images = explode(',', $product->products_image);
                        $store_images[] = $file_path;
                        $images = implode(',', $store_images);
                    }
                   
                    $updateProduct = DB::table('products')->where('id', $request->product_id)->update(array(
                        'products_image' => $images
                    ));
                    if($updateProduct)
                    {
                        $data = true;
                    }
                }
                
            }
        }
        return response()->json(['data' => $data]);
    }





    // GET IMAGE TOTAL
    public function get_total_product_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $product = DB::table('products')->where('id', $request->product_id)->first();
        }
        return view('vendor.common.ajax-product-edit-image', compact('product'));
    }






    public function delete_product_image_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $deleteImage = null;
            $key = $request->key;
            $product = DB::table('products')->where('id', $request->product_id)->first();
            if($product)
            {
                $images_array = explode(',', $product->products_image);
                if(array_key_exists($key, $images_array))
                {
                    $images = null;
                    $image_name = str_replace(' ','', $images_array[$key]);
                
                    if(!Image::delete($image_name))
                    {
                        return response()->json(['error' => ['image' => '*image could not be deleted!']]);
                    }

                    unset($images_array[$key]);
                    
                    if(count($images_array))
                    {
                        $images = implode(',', $images_array);
                    }
                    $data = $images;

                    $update_image = DB::table('products')->where('id', $request->product_id)->update(array(
                        'products_image' => $images
                    ));
                    if($update_image)
                    {
                        $data = true;
                    }
                    
                }
            }
        }
        return response()->json(['data' => $data]);
    }






    public function vendor_edit_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $validate = new Validate();
            $validation = $validate->validate([
                'name' => 'required|min:3|max:50',
                'price' => 'required',
                'price_slash' => 'required',
                'quantity' => 'required',
                'initial' => 'required',
                'detail' => 'required|min:3',
                'description' => 'required|min:3',
                'shipping' => 'required'
            ]);

            if($request->initial > $request->price)
            {
                return response()->json(['error' => ['initial' => '*Initial price must not be greater than price!']]);
            }

            if(!$validation->passed())
            {
                return response()->json(['error' => $validation->error()]);
            }else if($request->price > $request->price_slash)
            {
                return response()->json(['error' => ['price' => '*Price must not be greater than price slash!']]);
            }else{
               $sizes = $this->get_products_sizes($request);
               
               $is_feature = $request->feature ? $request->feature : 0;
               $is_special = $request->special ? $request->special : 0;
               $warranty = $request->warranty ? $request->warranty : null;
               $product_slug = DB::table('categories')->where('category_id', $request->category)->first();

               $update = DB::table('products')->where('id', $request->product_id)->update(array(
                                'vendor_id' => Session::get('vendor')['id'],
                                'products_name' => $request->name,
                                'brand_id' => $request->brand,
                                'category_id' => $request->category,
                                'products_slug' => $product_slug->category_name,
                                'products_price' => $request->price,
                                'products_quantity' => $request->quantity,
                                'products_price_slash' => $request->price_slash,
                                'initial_payment' => $request->initial,
                                'products_video_link' => $request->video_link,
                                'products_type' => $sizes,
                                'shipping_method' => $request->shipping,
                                'warranty' => $warranty,
                                'is_special' => $is_special,
                                'is_product_feature' => $is_feature,
                                'products_detail' => $request->detail,
                                'products_description' => $request->description,
                            ));

                $data = true;
            }
        }
        return response()->json(['data' => $data]);
    }






    public function vendor_delete_pending_product_ajax(Request $request)
    {
        if($request->ajax())
        {
            $data = false;
            $product = DB::table('products')->where('id', $request->product_id)->first();
            if($product)
            {
                $products_image = explode(',', $product->products_image);
                if(count($products_image) > 0)
                {
                    foreach($products_image as $image)
                    {
                        Image::delete($image);
                    }
                }
                $delete_products = DB::table('products')->where('id', $product->id)->delete();
                if($delete_products)
                {
                    $data = true;
                }
            }
        }
        return response()->json(['data' => $data]);
    }


    


    public function vendor_get_all_pending_product_ajax()
    {
        // get products
        $products = DB::table('products')->where('vendor_id', Session::get('vendor')['id'])->where('is_approve', 0)->leftJoin('brands', 'products.brand_id', '=', 'brands.brand_id')
        ->leftJoin('categories', 'products.category_id', '=', 'categories.category_id')->paginate(15);

        if(count($products) == '')
        {
        $products = null;
        }

        return view('vendor.common.ajax-pending-products-table', compact('products'));
    }




   

// end
}
